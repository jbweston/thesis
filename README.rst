============
 PhD Thesis
============
This is my PhD thesis entitled "Numerical methods for Time-resolved quantum nanoelectronics".
The directory structure is organised as follows::

    .
    ├── Makefile  --  Makefile for building the thesis
    ├── README.rst  --  this readme
    ├── references.bib  --  the bibliography
    ├── thesis.tex  --  the main TeX file
    ├── styles
    │  ├── cleanthesis.sty  --  general style options
    │  └── cover.sty  --  building the front page/cover
    ├── content  --  textual content, each chapter has its own TeX file
    │  ⋮
    │  └── ...
    ├── images  --  graphics, each chapter has its own subdirectory
    │   ├── cover  --  graphics for the cover
    │   ⋮   ⋮
    │   ⋮   └── ...
    │   └── ...
    └── docker  --  tools for working with the reproducible docker image
        ├── Dockerfile  --  dockerfile for the thesis build environment
        ├── build  --  script to build the thesis in the TeX container
        └── run  --  script to run an arbitrary command in the TeX container


The Build System
================
The thesis can be built by just saying::

    make

I use `XeTeX <https://en.wikipedia.org/wiki/XeTeX>`_, and hence require
the `xelatex` program to compile the thesis. In addition a number of
LaTeX packages are required. On Debian-derived systems it should be
enough to have the following Texlive packages installed:

- texlive-xetex
- texlive-luatex
- texlive-latex-extra
- texlive-fonts-extra
- texlive-math-extra
- texlive-lang-french
- texlive-science

The build process requires `inkscape <https://inkscape.org/>`_ to be
installed for converting the SVG images into PDF so that they can be
rendered by PdfTeX.

For the sake of reproducibility, a Docker image `jbweston/tex
<https://hub.docker.com/>`_ is provided. This image is available on
the Docker Hub, and the Dockerfile is also provided in `docker/Dockerfile`.
To build the thesis using the Docker image, simply execute::

    docker/build
