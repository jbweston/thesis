% -*- coding: utf-8 -*-

%\addcontentsline{toc}{chapter}{Conclusion}
\chapter{Conclusion}
\label{ch:con}

Experiments that probe the internal dynamics of quantum devices are beginning
to be realised in the laboratory, opening the way for a whole host of physical
effects qualitatively different to those found in static devices.  This thesis
has contributed to this effort along three main axes: improvements in numerical
algorithms, improvements in software tools, and new experimental proposals.

The first contribution involved taking the existing formalism developed in
ref.~\cite{gaury_numerical_2014} and improving it in two significant ways.  The
first improvement was the construction of an algorithm that scales as
$\order{Nt_{\mr{max}}}$  --- where $N$ is the number of degrees of freedom in
the central system, and $t_{\mr{max}}$ is the maximum simulation time --- as
opposed to the best effort of ref.~\cite{gaury_numerical_2014}, which has
$\order{Nt_{\mr{max}}^2}$ scaling. This was achieved by attaching slices of the
leads to the central system and incorporating non-Hermitian terms into the lead
Hamiltonian. The number of attached slices is tied to the required
\emph{accuracy} of the calculation, rather than being proportional to
$t_{\mr{max}}$ as in ref.~\cite{gaury_numerical_2014}. Secondly, the
integration over the initially filled states was moved from the energy domain
to the momentum domain, which regularises the singularities associated with new
modes opening in the leads.

The second contribution is an extension to the \kwant quantum transport
package, called \tkwant, that can handle time-dependent problems.  Although the
software is not yet of production quality, it has clearly already added
enormous value not only to our work, but also the work of collaborators.
In addition there is a clear set of steps that are required to bring
\tkwant to a standard where it can be released publicly alongside \kwant.

Using \tkwant we then investigated the propagation of a charge pulse inside a
flying qubit interferometer. The concept of dynamical control of interference,
recently developed in ref.~\cite{gaury_dynamical_2014}, allowed us to interpret
our results. The presence of this effect in this particular experimental setup
bodes well for an experimental verification of this dynamical control in the
near future. We then turned to superconducting systems and studied a Josephson
junction under the action of static and time-varying bias. We were able to
achieve \emph{quantitative agreement} between our calculation for the sub-gap
current-voltage characteristic of a short Josephson junction, and that obtained
using purely analytical methods based on Floquet theory. This indicates that
our time-resolved techniques are useful even for problems of a.c.\ transport
that would traditionally be treated using Floquet theory. The $\order{Nt_{\mr{max}}}$ scaling
of our numerical method really came into its own when investigating the
propagation of charge pulses inside long Josephson junctions, where the
infinite lifetime of the Andreev bound states means that extremely long
simulation times are needed.  Finally we combined the concepts of electronic
interferometers and superconductivity to investigate the effect of voltage
pulses on normal-insulator-normal-superconductor junctions in nanowires, which
exhibit Majorana states. Understanding such a junction as a Fabry-Perot
interferometer, we were able to see that repeated application of voltage pulses
\emph{stabilises the dynamical modification of the interference pattern}.  This
allowed us to perform \enquote{spectroscopy} on the Majorana states, which
provides a signature of their nature as resonant Andreev states.

\subsubsection{Future Perspectives}
Time-resolved quantum electronics is still an emerging field and one that will,
we think, see a heyday in the coming years. In particular, the use of
single-electron sources in a new generation of
experiments is a particularly exciting prospect.  In this endeavour we can
envisage two main axes for development in terms of numerical tools.
Firstly, \tkwant (the software) needs to be brought to the stage where it is
just as easy to set up and test new ideas for time-resolved transport as it is
to use \kwant for stationary transport. This will allow theorists to more
rapidly propose interesting experiments and help them gain an intuitive
understanding of a new physical system before they bring analytical techniques
to bear.  Secondly, in order to obtain greater parity with experiment, \tkwant
(the algorithm) should be modified to allow for a self-consistent calculation
of observables that can be fed back into the Hamiltonian.  The most obvious
example would be to self-consistently solve the Poisson equation in order to account for
electron-electron interactions on the mean-field level.  Another interesting
possibility would be to embed a Josephson junction into its surrounding
(classical) circuit.  This would allow us to go beyond simple models (such as
RCSJ) by treating the full quantum dynamics of the Josephson junction. There
are significant challenges to overcome, however, as the self-consistency
effectively couples the wavefunctions at different energies, rendering the
problem non-linear and making an efficient parallel implementation much more
difficult. Despite the challenges, such developments would bring with them a
wealth of new possibilities and, ultimately, new physics.
