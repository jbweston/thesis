% -*- coding: utf-8 -*-
%
\chapter{Split Wire Flying Qubit}
\label{ch:fly}
\def \imgpath {images/flying-qubit/}
\graphicspath{{\imgpath}}

In recent years there has been a big push to develop platforms for quantum
computation. Many of the common proposals encode the quantum information (or
\emph{qubits}) in localised states (e.g.\ on a quantum
dot~\cite{loss_quantum_1998, veldhorst_two-qubit_2015}, or in a superconducting
junction~\cite{vion_manipulating_2002, chen_qubit_2014, richer_circuit_2016,
barends_superconducting_2014}). An alternative proposal, referred to as a
\enquote{flying qubit} consists of encoding the information in a state with a
finite velocity~\cite{bertoni_quantum_2000}, so that the qubits can have gate
operations applied \emph{in flight} as they are moved around to different parts of the circuit.
Recently a split wire geometry proposed to host and manipulate these flying
qubits was realised experimentally, and has since seen an increased
experimental as well as theoretical interest~\cite{takada_measurement_2015,
takada_transmission_2014, bautze_theoretical_2014, yamamoto_electrical_2012}.
Previous efforts to simulate this system were limited to looking at d.c.\
physics~\cite{bautze_theoretical_2014}, or did not take into account the Fermi
statistics of the electrons in the system~\cite{bertoni_quantum_2000}.
Experiments currently being carried out in the group of Christopher Baüerle at
the Néel Institute in Grenoble hope to
directly probe the time-resolved behaviour of such a split-wire flying qubit.
With the source-sink algorithm we are perfectly positioned to numerically
investigate the behaviour of these flying qubits in the time domain, which will
aid in interpretation of experimental data and design of future generations of
devices. In this chapter we will start by recovering previously obtained
results for the d.c.\ behaviour of the split wire system, in order to
illustrate the main physical effect at play: quantum interference between
different paths through the system.  This will be followed by \emph{novel
time-resolved simulations} when a voltage pulse is applied to one of the
electrodes of the split wire. We will see the emergence of a \emph{dynamical
modification} of the interference compared to the d.c.\ case, a concept that
was recently elucidated in a number of
publications~\cite{gaury_dynamical_2014,gaury_.c._2015}.


\section{Simple Model for the Split Wire Geometry}
\label{sec:fly:model}
\Cref{fig:fly:setup}a shows the experimental setup that we are going to model.
The setup consists of a gallium arsenide-aluminium gallium arsenide
heterostructure (dark background) with metallic gates (the light grey shapes)
deposited on the surface. A two-dimensional electron gas (2DEG) forms at the
interface between the two materials (parallel to the page), and ohmic contacts
(on the extreme right/left of the sample, out of the view of
\cref{fig:fly:setup}a) allow for charge to be injected into the 2DEG. Voltages
applied to the metallic gates allow the charge to be confined to restricted
regions of the device. We shall be interested in the region of the device
outlined in \cref{fig:fly:setup}a, which we model as two quasi one-dimensional
wires of width $W$ that are coupled in some finite region; this is shown in
\cref{fig:fly:setup}b. There is a potential barrier between the two wires
controlled by the parameter $V_T$; this controls the tunnelling between the two
wires.  While the wires in our model are formally coupled over a length $L$, we
actually reduce the potential adiabatically along the $x$ direction from a
large value $V_\infty$ to $V_T$ (the purple colour gradient in
\cref{fig:fly:setup}b) in order to minimise the reflection from the boundary
between the coupled/uncoupled regions. The consequence of this is that the
\emph{effective} length over which the wires are coupled is smaller,
$\tilde{L}$. In addition we model the potential provided by the side-gates
(light grey in \cref{fig:fly:setup}a) as a hard-wall boundary in the $y$
direction, in addition to a uniform potential in the coupled region.  This
potential is uniform over the coupled region, controlled by $V_b$,  and falls
adiabatically to zero outside the coupled region (the grey colour gradient in
\cref{fig:fly:setup}b); this allows us to control the number of open conduction
channels in the coupled region. We ground three of the contacts and apply a
(possibly time-dependent) voltage $V_p$ to lead $\uparrow$ on the left.  We
model the voltage drop as being abrupt between the contact and the scattering
region.

\begin{figure}[tb]
    \centering
    \includegraphics[width=0.9\textwidth]{split_wire}
    \scaption{Split wire setup and transverse modes. (a) Scanning electron
    microscope image of the experimental setup (reprinted with permission from
    ref.~\cite{bautze_theoretical_2014}
    \href{http://dx.doi.org/10.1103/PhysRevB.89.125432}{Copyright} 2015 by the
    American Physical Society). (b) Sketch of our model setup, the gate voltage
    $V_T$ controls the tunnel barrier, $V_b$ controls the charge density in the
    coupling region, and $V_p$ applied to the upper-left lead can inject charge
    into the system. $L$ measures the total coupling region length, $\tilde{L}$
    measures the effective coupling region length, and W measures the width of
    an individual wire. The yellow dashed lines labelled (c) and (d) refer to
    the cross sections shown in the remaining subfigures. (c) Sketch of the 4
    lowest energy trans- verse modes before the coupled region. along with the
    transverse potential, $V(y)$. The states $\ket{n, ↑}$ and $\ket{n, ↓}$ are
    degenerate. (d) Sketch of the 4 lowest energy transverse modes in the
    coupled region, along with the transverse potential, $V(y)$.
    }
    \label{fig:fly:setup}
\end{figure}

As the Fermi wavelength is sufficiently long compared to the inter-atomic
distance of the material ($λ_F \approx \SI{44}{\nano\metre}$ for the
electron density used in the sample of ref.~\cite{bautze_theoretical_2014})
we can model the system with a continuum Hamiltonian:
\begin{equation}
\begin{split}
    \qop{\ham}(t) = \int_{-\infty}^{\infty} \dd{x} \int_{-W}^{W} \dd{y}
    \qop{ψ}^\dagger(x, y)
    \bigg[&-\frac{\hbar^2}{2m^*}\nabla^2 + qU_T(x)δ(y) + qU_b(x)\\
          &+ qU_p(t)Θ(x - M)Θ(-y)\bigg]
    \qop{ψ}(x, y),
\end{split}
\end{equation}
where $m^*$ is the effective mass of the 2DEG, $q$ is the electronic charge, $M
= L/2 + 5χ$ ($χ$ is a length that determines the scale of the variations in
$U_b(x)$ and $U_T(x)$) and $Θ(y)$ and $δ(y)$ are Heaviside functions and Dirac
delta functions respectively. The $\cqop{ψ}(x,y)$
($\qop{ψ}(x,y)$) are creation (destruction) operators for single particle
states at position $(x,y)$. The potentials take the following form:
\begin{align}
    U_T(x) &= V_T + V_{\infty}\qty{1 - \frac{1}{2}\qty[
        \tanh\qty(\frac{x + L/2 - 5χ}{χ}) +\tanh\qty(\frac{x - L/2 + 5χ}{χ})]} \\
    U_b(x) &= \frac{V_b}{2}
        \qty[\tanh\qty(\frac{x + L/2 + 5χ}{χ}) - \tanh\qty(\frac{x - L/2 - 5χ}{χ})],
\end{align}
where $V_{\infty}$ is a value sufficiently large so as to render the leads
effectively uncoupled just at the start of the coupled region (to enable
a smooth transition between these regions).
We discretise this Hamiltonian onto a square lattice of spacing $a$,
using the procedure outlined in \cref{sec:app:discretising}, and
perform a gauge transformation to bring the time-dependence
from the lead $\uparrow$ on the left into the coupling between the lead
and the scattering region (see \cref{sec:app:gauge}
for details). In what follows we will express all energies
in units of the tight-binding bandwidth $γ_B=4ℏ^2/(2m^* a^2)$, voltages in units of $γ_B/e$ and times
in units of $ℏ/γ_B$.


\section{d.c.\ Characterization of the Split Wire}
\label{sec:fly:dc}
In this section we will characterise the d.c.\
behaviour of the device. We shall see
that the split wire can be considered as an effective two-path interferometer;
this point of view will be invaluable when interpreting the time-resolved
simulations in \cref{sec:fly:ac}.
In order to calculate the d.c.\ conductance
$G_{σ'\,σ}$ between lead $σ$ on the left and lead $σ'$ on the right
($σ\in\{\uparrow,\downarrow\}$) we need only use the Landauer
formula~\cite{landauer_spatial_1957,landauer_electrical_1970}:
\begin{equation}
    \label{eq:fly:landauer}
    G_{σ'\,σ} = \frac{2e^2}{h} D_{σ'\,σ}
\end{equation}
where $D_{σ'\,σ}$ is the transmission from lead $σ$ on the left
to lead $σ'$ on the right, defined by
\begin{equation}
    D_{σ'\,σ} = \sum_{n,m} T_{mσ',nσ}
\end{equation}
where $T_{mσ',nσ}$ is the transmission \emph{probability} from
mode $\ket{n,σ}$ on the left to mode $\ket{m,σ'}$ on the right
(these modes are sketched in \cref{fig:fly:setup}c). In all that
follows we shall assume that inter-band scattering is negligible, i.e.
$T_{mσ',nσ} = \delta_{mn} T_{nσ',nσ}$ where $\delta_{mn}$
is the Kronecker delta.

\subsection{Analytical Treatment Using Scattering Theory}
\label{sec:fly:dc-analytics}
The $T_{nσ',nσ}$ can be calculated by using
a wave matching procedure; here we will reproduce previously
perfomed calculations~\cite{bautze_theoretical_2014} to highlight
the salient physics of the system, while avoiding the intricacies
of the more complicated model presented in \cref{sec:fly:model}.
In \cref{sec:fly:dc-numerics} we will treat the model numerically,
which will allow us to validate this simplified analytical treatment.

The full wavefunction in the uncoupled region can be written $Ψ_{n,
σ}(x, y) = \braket{y}{n,σ} e^{ik_{n,σ}x}$, where $σ \in
\{\uparrow, \downarrow\}$, and its energy is $E = E_{n,σ} +
(\hbar^2/2m^*)k_{n,σ}^2$, where $E_{n,σ}$ is the energy of the transverse mode
$\ket{n, σ}$, and $k_{n,σ}$ is the longitudinal wavevector.  As the states
$\ket{n, \uparrow}$ and $\ket{n, \downarrow}$ are degenerate for a given $n$,
we can also define symmetric and antisymmetric superpositions:
\begin{equation}
\begin{split}
    \ket{n, \uparrow} &= \frac{1}{\sqrt{2}}\qty[\,\ket{n, S_u} + \ket{n, A_u}]\\
    \ket{n, \downarrow} &= \frac{1}{\sqrt{2}}\qty[\,\ket{n, S_u} - \ket{n, A_u}],
\end{split}
\end{equation}
where the $u$ subscript reminds us that these are transverse modes in the
\emph{uncoupled} region. In the coupled region we also have symmetric and
antisymmetric modes $\ket{n, S}$ and $\ket{n, A}$ (illustrated in
\cref{fig:fly:setup}d), and we suppose that the transition from the uncoupled
to the coupled region is adiabatic, such that $\ket{n, S_u}$ evolves into
$\ket{n, S}$ and $\ket{n, A_u}$ evolves into $\ket{n, A}$ with no inter-mode
scattering. While $\ket{n, A_u}$ and $\ket{n, S_u}$ are degenerate, $\ket{n,
A}$ and $\ket{n, S}$ are not. This means that for a given energy the states
will have different longitudinal wavevectors, $k_{n, A}$ and $k_{n, S}$. If we
are in a state $\ket{n, \uparrow}$ in the uncoupled region on the left,
this means that a length $\tilde{L}$ after the wires are coupled we will be in a state:
\begin{equation}
    \ket{ψ_{n, \uparrow}} = \frac{1}{\sqrt{2}}
    \qty[e^{ik_{n, A}\tilde{L}}\ket{n, A} + e^{ik_{n, S}\tilde{L}}\ket{n, S}].
\end{equation}
The wires are then adiabatically uncoupled (near the
right-hand leads) and we can write the state as the following decomposition
on the states $\ket{n, ↑}$ and $\ket{n, ↓}$ on the right:
\begin{equation}
    \ket{ψ_{n, \uparrow}} = \frac{1}{2} \qty[
        \qty(e^{ik_{n, S}\tilde{L}} + e^{ik_{n, A}\tilde{L}})\ket{n, \uparrow} +
        \qty(e^{ik_{n, S}\tilde{L}} - e^{ik_{n, A}\tilde{L}})\ket{n, \downarrow}
    ].
\end{equation}
We immediately see that the difference in wavevectors will give rise to
interference between the symmetric and antisymmetric components.  We can thus
write down the transmission \emph{amplitudes} for arriving on the right in
$\ket{n, \uparrow}$ or $\ket{n, \downarrow}$ given that we were
injected on the left in $\ket{n, \uparrow}$:
\begin{equation}\label{eq:fly:transmission}\begin{split}
    t_{n\uparrow,n\uparrow} &=
        \exp\qty(i\frac{k_{n, A} + k_{n, S}}{2}\tilde{L}) \cos\qty(\frac{\Delta k_n}{2}\tilde{L})\\
    t_{n\downarrow,n\uparrow} &=
        i\exp\qty(i\frac{k_{n, A} + k_{n, S}}{2}\tilde{L}) \sin\qty(\frac{\Delta k_n}{2}\tilde{L}),
\end{split}\end{equation}
where $\Delta k_n = k_{n, S} - k_{n, A}$.  The transmission probabilites can be
calculated from these amplitudes using
$T_{nσ',nσ} = \abs{t_{nσ',nσ}}^2$.


\subsection{Numerical Treatment}
\label{sec:fly:dc-numerics}
Having an intuitive picture of the physics at play in the system, we shall
now numerically study the model presented in \cref{sec:fly:model} using the
\kwant~\cite{groth_kwant:_2014} package. In addition to providing a visualisation
of the concepts developed in \cref{sec:fly:dc-analytics} it will also allow
us to verify that our model conforms to this simplified view.

Figure~\ref{fig:fly:T_Vg} shows how the wavevector difference changes as a function
of the coupling gate voltage $V_T$ and the effect that this has on the
transmission $D_{\uparrow\,\uparrow}$ from lead $\uparrow$ on the left to lead
$\uparrow$ on the right. We clearly see regular oscillations when the
wavevector difference changes linearly. As we go to to very high gate voltages
we effectively uncouple the two wires, which explains why
$D_{\uparrow\,\uparrow} \to 1$ in this limit. The red dashed line in
fig.~\ref{fig:fly:T_Vg}a shows $D_{\uparrow\,\uparrow}$ calculated using
eq.~\eqref{eq:fly:transmission} where $\Delta k_0$ has been calculated numerically
from the tight binding model; we see a good fit between the model and the simple
analytical result.

\begin{figure}[tb]
    \centering
    \includegraphics[width=0.7\textwidth]{T_Vg}
    \scaption{d.c.\ simulation of split wire
		with $L=700a$, $W=10a$, $V_b=0.11γ_B/e$ and $E_F=0.15γ_B$. At this Fermi
        energy only the modes $\ket{0, \uparrow}$ and $\ket{0, \downarrow}$ in
        the coupled region are open. (a) Black full line: transmission calculated
        from tight-binding simulation, red dashed line: transmission calculated
        using eq.~\eqref{eq:fly:transmission} with $\Delta k_0$ calculated from
        tight-binding and $\tilde{L}$ as a fitting parameter. We used $\tilde{L} = 1242a$.
        (b) Tight-binding calculation of the difference in momentum between
        symmetric and antisymmetric modes in the coupling region. Both plots
        share the x-axis $V_T$ scale.
    }
    \label{fig:fly:T_Vg}
\end{figure}


Figure~\ref{fig:fly:T_Ef} shows the dispersion relations for the leads
(subfigure a) and in the coupled region (subfigure b) calculated from
the tight-binding model. We see in fig.~\ref{fig:fly:T_Ef}c the transmission
probabilities for being transmitted through the first and second modes
from lead $\uparrow$ on the left to lead $\uparrow$ on the right.
We see that the transmission probabilities are 0 before the
corresponding modes in the central region open. Note that
in order for $T_{nσ',nσ}$ to be different from 0 we need
\emph{both} modes $\ket{n,A}$ \emph{and} $\ket{n,S}$ to be open in the
coupled region, as $\ket{n, σ}$ is a linear combination of
both. We see that the transmission probabilities oscillate as a function
of energy. The reason for this is clear, as fig.~\ref{fig:fly:T_Ef}b clearly
shows that $\Delta k_n$ changes as a function of energy.
The inter-band transmission probabilities $T_{mσ',nσ}$ (with
$m \ne n$) are not shown, but are 0 at all energies (validating the assumptions
of the analytical derivation above); this is because
the transition from uncoupled to coupled region is done in an adiabatic
manner.

\begin{figure}[tb]
    \centering
    \includegraphics[width=\textwidth]{T_Ef}
    \scaption{
        Dispersion relations and transmissions for the split wire
        with $L=700a$, $W=10z$, $V_T=0.27γ_B/e$ and $V_b=0.11γ_B/e$.
        (a) Dispersion relation in lead $\uparrow$ showing the three lowest
        energy modes.
        (b) Dispersion relation in the coupling region of the split wire,
        showing the first 2 symmetric (full lines) and anti-symmetric (dashed lines) modes.
        (c) Transmission probability from $\ket{0,\uparrow}$ on the left to
        $\ket{0, \uparrow}$ on the right (black full line); transmission probability from
        $\ket{1,\uparrow}$ on the left to $\ket{1, \uparrow}$ on the right
        (red dotted line, shifted by 1 for clarity); total transmission from the $\uparrow$
        lead on the left to the leads on the right (green dashed line).
        All three plots share the y-axis energy scale.
    }
    \label{fig:fly:T_Ef}
\end{figure}

One last point, which is perhaps a bit subtle, is that we expect to be able to
see these interference effects even with a \emph{large} number of open
channels. Indeed, at the energy where the $n+1$ channel opens the $\Delta
k_{n+1}$ is much larger than the $\Delta k_n$ at the same energy (see
\cref{fig:fly:T_Ef}b). This means that $T_{nσ',nσ}$ oscillates much slower than
$T_{n+1\,σ',n+1\,σ}$ at the same energy, as can be clearly seen in
\cref{fig:fly:T_Ef}c (at a given energy the red curve is oscillating much more
quickly than the black curve). This separation in frequency of the different
$T_{nσ',nσ}$ means that the oscillations from the different channels will be
clearly distinguishable in the full differential conductance $G_{σ'\,σ}$.



\section{Application of a Voltage Pulse}
\label{sec:fly:ac}
Now that we have the understanding of the system in d.c.\ we can now turn to
time-resolved simulations. We apply a Gaussian voltage pulse to lead $\uparrow$
on the left and measure the current $I_\uparrow$ ($I_\downarrow$) leaving the
system on the right via lead $\uparrow$ ($\downarrow$). We also measure the
current $I_{\mathrm{\mr{in}}}$ injected into the system by the voltage pulse. We
assume that the voltage drop is sharp and localised at the system-lead
boundary. In addition, we tune $V_b$ such that only the modes $\ket{0, A}$ and
$\ket{0, S}$ are open in the coupled region at the Fermi energy.
The pulses we apply have a full-width at half maximum of $200 ℏ/γ_B$ and a
typical height of $0.03γ_B/e$.
%
The d.c.\ transmissions at the Fermi energy for the setup are
$D_{\uparrow\,\uparrow} = 0.1$ and $D_{\downarrow\,\uparrow} = 0.9$. We have
$L=700a$ and $W=10a$; in total we have $16700$ sites in the scattering region.

\begin{figure}
    \centering
    \includegraphics[width=0.9\textwidth]{nbar_colormap}
    \scaption{Snapshots of the excess particle density in the split wire after
        the application of a voltage pulse that injects $\bar{n} = 0.1$
        particles on average.  Red (blue) indicates positive (negative)
        deviations from the equilibrium particle density. The colour scale is
        independently normalised to the maximum density in each snapshot.
    }
    \label{fig:fly:td_charge}
\end{figure}

Figure~\ref{fig:fly:It} shows the results of a simulation where the above-defined
currents are measured. Due to the large characteristic length $1/\Delta k_{0}$,
and hence the large length of the system, we need to go to very long times
($9500$ times the inverse hopping parameter) in order to see the output current.
The voltage pulse shown injects an average of $\bar{n}=2$ particles into
the system, where
\begin{equation}
    e\bar{n} = \int_0^\infty \dd{t}\, I_{\mr{in}}(t)
\end{equation}
and $e$ is the electronic charge. We clearly see that the output current
oscillates between the $\uparrow$ and $\downarrow$ leads, which is
counterintuitive; naïvely one would expect that the current in the two leads
would have the same \enquote{shape} as a function of time, and that only the
magnitudes would be different (proportional to the d.c.\ transmission).

\begin{figure}
    \centering
    \subcaptionbox{\captionstyle
            Current as a function of time flowing in: lead $\uparrow$ on the
            left ($I_{\mr{in}}$), lead $\uparrow$ on the right ($I_\uparrow$), and
            lead $\downarrow$ on the right ($I_\downarrow$), for a voltage pulse
            that injects $\bar{n} = 2.0$ particles.\label{fig:fly:It}
        }[0.49\linewidth]{\includegraphics[width=0.42\textwidth]{I_t}}
    \hfill
    \subcaptionbox{\captionstyle
            Number of transmitted particles on the right in lead $\uparrow$
            ($n_\uparrow$) and lead $\downarrow$ ($n_\downarrow$) as a function
            of the injected number of particles ($\bar{n}$). Symbols:
            time-resolved simulation, dashed line: application of
            eq.~\eqref{eq:fly:td_nt} with $\tilde{L}=596a$ as a fitting
            parameter.\label{fig:fly:nt_nbar}
        }[0.49\linewidth]{\includegraphics[width=0.42\textwidth]{nt_nbar}}
    \scaption{
        Charge transport after application of a voltage pulse on lead
        $\uparrow$ on the left of the split-wire.  The system has
        $L=700a$, $W=10a$, $V_b=0.11γ_B/e$,
        $V_T=0.1446γ_B/e$ and $E_F=0.15γ_B$. We use a pulse with a duration (full-width
        half-maximum) of $200 ℏ/γ_B$. In d.c.\ the
        transmissions at the Fermi energy are $D_{\uparrow\,\uparrow} = 0.9$
        and $D_{\downarrow\,\uparrow} = 0.1$.
    }
    \label{fig:fly:td_sims}
\end{figure}

Figure~\ref{fig:fly:nt_nbar} shows the number of particles transmitted on the right
into lead $\uparrow$ ($n_\uparrow$) and $\downarrow$ ($n_\downarrow$) as a
function of the number of injected particles. Rather than a simple
proportionality relationship (where the slope would be given by the d.c.\
transmission), we see that the number of particles depends non-linearly on
$\bar{n}$ and even \emph{oscillates} with $\bar{n}$. This curious behaviour can
be understood within the framework of \emph{dynamical control of interference},
aspects of which were elucidated in a number of recent
publications~\cite{gaury_dynamical_2014,gaury_.c._2015}. To understand this,
one has to remember that there are already electrons in plane-wave states filling the Fermi sea
before the voltage pulse is applied. The naïve picture that the electronic
wavefunction will look like some sort of Gaussian wavepacket after the application
of the voltage pulse is essentially wrong. Instead, the voltage pulse actually
puts a \emph{twist} in the phase of the plane-wave states occupying the Fermi
sea.
%
To illustrate this, let us take a simple case where we have a perfect, infinite
one-dimensional system with a time-dependent potential applied at $x<0$. Let us
take the case where the potential is initially zero and is abruptly raised to
$V$ at $t=0$ and then lowered to 0 again at some later time $t_1$. Initially
the scattering states originating from the left below the Fermi energy take the
form of plane waves $ψ^{st}_{αE}(t) = \exp(ik(E)x - iEt/ℏ)$. Just after the
potential is raised the part of the wavefunction at $x<0$ will now be
oscillating \emph{faster}, with frequency $(E+V)/ℏ$. As time passes, the part
of the wavefunction that oscillates more quickly will propagate into the $x>0$
region at a velocity $(1/ℏ)∂E/∂k$. When the potential is again lowered at $t=t_1$
the part of the wavefunction under the voltage pulse will again oscillate at
frequency $E/ℏ$, however the part that propagated into the $x>0$ region will
still be oscillating at frequency $(E+V)/ℏ$. This is illustrated in
\cref{fig:fly:pulse-propagation}. The phase of the wavefunction before and
after the pulse are therefore offset by $(e/h) V t_1$ with respect to one
another (more generally they are offset by $φ = \int \dd{t} (e/h) V(t)$ for voltage pulses $V(t)$);
the pulse induces a \enquote{phase domain wall} in the wavefunction.  While this
explanation is not rigorous, it can be shown that this intuitive picture is
correct in the limit that the spectrum is linear (no
dispersion) on energy scales of $\order{V}$ around
the Fermi energy~\cite{gaury_numerical_2014}.
\begin{figure}
    \centering
    \includegraphics[width=0.7\textwidth]{phase_change}
    \scaption{Sketch of the effect of a voltage pulse $V(x, t) = v(t)u(x)$ on a
        plane-wave $\exp(ikx - iEt/ℏ)$. The $v(t)$ and $u(x)$ show the profile of
        the voltage pulse in time/space.  The orientation of the arrows shows
        the phase of the wavefunction relative to a plane-wave $\exp(ikx - iEt)$.
        When the voltage pulse is applied, the wavefunction oscillates faster
        in time, and this phase \enquote{twist} $φ$ propagates to the right.
    }
    \label{fig:fly:pulse-propagation}
\end{figure}
%
We can now employ this \enquote{phase domain wall} (PDW) picture to the present case
to understand the source of the oscillations of $n_\uparrow$ ($n_\downarrow$)
with $\bar{n}$. The voltage pulse creates a PDW that propagates into the split
wire system. In the coupled region the state $\ket{0, S}$ has a larger velocity
than $\ket{0, A}$ at a particular energy, which means that the PDW will travel
faster along the antisymmetric component of the wavefunction than the symmetric
one. If the pulse is sufficiently short with respect to $Δτ_F = L/(v_{0,S} -
v_{0,A})$ then the antisymmetric component will have its phase modified by $φ$
at the output leads \emph{before} the symmetric component, and the interference
pattern will be modified during this finite interval, before returning to the
d.c.\ interference pattern once the PDW has arrived from the (slower) symmetric
component. \Cref{fig:fly:pdw} illustrates this. The number of particles
transmitted into each of the leads will therefore be affected by this
modification of the interference pattern.
\begin{figure}
    \centering
    \includegraphics[width=0.5\textwidth]{paths}
    \scaption{Sketch of the propagation of the phase domain wall (PDW) along the
        two \enquote{paths} (symmetric $\ket{0, S}$ and antisymmetric $\ket{0, A}$
        components) in the coupled region of the split wire.
        Between $t=L/v_{0,S}$ and $t=L/v_{0,A}$ the $\ket{0, S}$ component
        at the output on the right has had its phase modified, but the
        $\ket{0, A}$ component has not. The interference between the two
        components here will be different to that in d.c.; the interference
        has been modified \emph{dynamically}.
    }
    \label{fig:fly:pdw}
\end{figure}
In ref.~\cite{gaury_dynamical_2014} this reasoning is made more precise; in
fact, as the split wire constitutes a two-path interferometer, the analysis for
the present case is identical to that of the Mach-Zehnder interferometer
studied in ref.~\cite{gaury_dynamical_2014}. Concretely, we can apply eqs.~(27)
and (28) of ref.~\cite{gaury_dynamical_2014} to the split wire system and
obtain the following relations:
\begin{align}
    \label{eq:fly:td_nt}
    n_\uparrow = \frac{\bar{n}}{2} \qty[1 +
        \frac{1}{π}\sin(π\bar{n})\cos\qty(π\bar{n} + \frac{Δk_0}{2}\tilde{L})]\\
    n_\downarrow = \frac{\bar{n}}{2} \qty[1 -
        \frac{1}{π}\sin(π\bar{n})\cos\qty(π\bar{n} + \frac{Δk_0}{2}\tilde{L})].
\end{align}
The lines in \cref{fig:fly:nt_nbar} correspond to the above analytical result
where $\tilde{L}$ is used as a fitting parameter (we used $\tilde{L}=596a$); we
see a very good agreement with the numerics.
