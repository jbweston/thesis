% -*- coding: utf-8 -*-
%
\def \imgpath {images/}
\graphicspath{{\imgpath}}
\renewcommand{\thefigure}{\arabic{figure}}
\addcontentsline{toc}{chapter}{Summary of the Thesis}
\graphicspath{{\imgpath}}

\chapter*{Summary of the Thesis}
\addtocontents{toc}{\protect\setcounter{tocdepth}{0}}
\label{ch:sum}

This thesis deals with the problem of simulating and understanding transport of
electrons in quantum devices when the device is subjected to time-dependent
perturbations such as pulses of voltage on contacts or electrostatically coupled
gates, electromagnetic radiation etc.
This thesis consists of two main parts. The
first part is dedicated to the advances in theoretical techniques and their
implementation as numerical algorithms that allow us
to perform simulations where the time to solution scales linearly in the system
volume and maximum (simulation) time required. This is an improvement over the
present state of the art, which scales linearly in system volume, but as the
\emph{square} of the simulation time. The second part applies this algorithm to
several key physical devices: a flying qubit interferometer, a Josephson
junction under bias, and a nanowire coupled to a superconductor, which exhibits
Majorana states at its extremities. Here we shall briefly present the key
results from each chapter.

\subsection*{\color{ctcolorblack}Chapter \ref{ch:qt}:
             \color{ctcolorsection} Introduction to Quantum Transport in the Time Domain}
We start by introducing the field of quantum transport, including typical
length scales, and look at the archetypical quantum device: a two-dimensional
electron gas at the interface between two semiconductors. We then go on to look
at recent experiments involving single electron sources, which highlight the
need for a better understanding of the titular quantum transport in the time
domain. We follow this with a brief review of the currently available numerical
techniques: non-equilibrium Green's functions and wavefunction based
approaches. This chapter does not contain any new results, but serves as an
introduction to what follows.

\subsection*{\color{ctcolorblack}Chapter \ref{ch:num}:
             \color{ctcolorsection} Numerical Algorithms for Time-Resolved Quantum Transport}
In this chapter we present the improvements that have been made to
the numerical algorithms with respect to the current state of the art. We start
off by covering known material (calculation of stationary scattering
wavefunctions) before presenting our approach to time-resolved transport. This
consists of starting with initial scattering states of the system and then
evolving them in time using the time-dependent Schrödinger equation;
observables are then calculated by integrating over the contributions from
these time-evolved scattering states. In the end the differential equations to
solve are
\begin{equation}
\begin{split}
    i\pdv{t}\bar{ψ}_{αE}(t) &= [\mb{H}(t) - E]\bar{ψ}_{αE}(t) +
                              \underbrace{\mb{W}(t) ψ^{\mr{st}}_{αE}}_
                                         {\mr{source\ term}} -
                              \underbrace{i\mb{Σ} \bar{ψ}_{αE}(t)}_
                                         {\mr{sink\ term}}
    \\
    ψ_{αE}(t) &= \qty[\bar{ψ}_{αE}(t) + ψ^{\mr{st}}_{αE}]e^{-iEt},
\end{split}
\end{equation}
where $ψ_{αE}(t)$ is the wavefunction in which we are interested,
$ψ^{\mr{st}}_{αE}$ is the initial scattering state incoming in channel
$α$ and energy $E$, $\mb{H}(t)$ is the Hamiltonian matrix, $\mb{W}(t) =
\mb{H}(t) - \mb{H}(0)$, and $\mb{Σ}$ is a diagonal matrix that is non-zero
in a finite number of cells of the leads that are attached to the central
scattering region.
%
The key difference from previous approaches is the use of a \emph{complex
absorbing potential}, $-i\mb{Σ}$, to handle the boundary conditions at the
system-lead interface.  This is what gives our algorithm linear scaling with
system volume and simulation time. The characteristic \enquote{source} and
\enquote{sink} terms in the Schrödinger equation lead us to dub this the
\enquote{source-sink} method.

In addition we perform a detailed analysis
of the effect of the complex potential, including an analytical calculation of
the reflection amplitude beyond the WKB approximation. We find that the
reflection from the complex potential satisfies
\begin{equation}
\begin{split}
    r_Σ(E) = i&e^{2ikL}e^{-Ak/E} +\\
    &\frac{1}{4iEL}∫_0^\infty Σ'(u) \exp\qty{2ikLu - \frac{k}{E}∫_0^u Σ(v) \dd{v}} \dd{u}
    + \order{\qty(\frac{1}{kL})^2},
\end{split}
\end{equation}
where $Σ(x)$ is the absorbing potential as a function of distance into the absorbing
region, $L$ is the length of the absorbing region, and $k$ is the wavevector
corresponding to energy $E$.

Finally, we discuss how the observables can be calculated by integrating the
contributions from the wavefunctions, weighted by the appropriate Fermi-Dirac
distribution. We propose to \emph{integrate in momentum space}, as opposed to
the energy-space integrations used by competing methods,  to avoid
singularities arising from new modes opening in the leads of the system. We
provide two examples that illustrate that the momentum-space integration
requires drastically fewer points (and hence fewer wavefunctions to evolve)
than the corresponding energy-space integration.


\subsection*{\color{ctcolorblack}Chapter \ref{ch:soft}:
             \color{ctcolorsection} Software Design}
In this chapter we  discuss the requirements for a robust software tool that
implements the algorithms discussed in the preceding chapter.  We start by
motivating the need for solid abstractions of the fundamental mathematical
objects, as opposed to an \enquote{all-singing all-dancing} monolithic code. We
then take the example of the \kwant package and illustrate --- using a simple
toy example --- how it implements this philosophy for time-\emph{independent}
transport.  In particular we put emphasis on how \kwant enables one to express
a problem to solve \emph{in terms of the mathematical structure}, instead of in
terms of its low-level representation to the computer.

We move on to discuss the extra pieces that would have to be
implemented on top of \kwant in order to be able to handle time-dependent
problems, we dub these \enquote{extra pieces} \tkwant, for
\enquote{time-dependent \kwant}. We finish by showing a gallery of examples
where \emph{the current implementation of \tkwant has been used}, outside of
the applications studied in this thesis: calculating time-resolved shot noise;
stopping electrons in the quantum Hall regime; a universal transient regime for
voltage pulses applied to interferometers; and simulating Floquet topological
insulators.  This includes work done outside of the research group of the
author, which indicates that -- despite its flaws -- the current implementation
is nevertheless \emph{providing value} to research projects.

\subsection*{\color{ctcolorblack}Chapter \ref{ch:fly}:
             \color{ctcolorsection} Split Wire Flying Qubit}
We now move to the second half of the thesis, which is concerned with specific
applications of the aforementioned algorithms and software tools. The first
application is to a split-wire setup implemented in a two-dimensional
electron gas, which consists of two quasi one-dimensional regions separated by
a controllable tunnelling barrier.
%
This device has been proposed as an implementation of a \enquote{flying qubit},
where the state of an electron is modified as it is moved around the quantum
circuit. This device is currently being implemented experimentally in the group
of Christopher Bäuerle at the Néel Institute in Grenoble.

We start by treating the problem in the absence of any time dependence, using a
scattering approach. This allows us to appreciate that the system acts as an
\emph{interferometer} with the symmetric and antisymmetric states in the split
wire providing the two alternative paths through the system.  We follow by
applying a \emph{pulse of bias voltage} to the split wire. We see that the
number of charges recovered on the other side of the device \emph{oscillates}
with the number of charges sent by the voltage pulse, as shown in
\cref{fig:sum:fly}.  Experimentally this would correspond to a measurement of
the average current when the voltage pulse is repeated in time.
%
This effect is interpreted within the paradigm of \emph{dynamical control of
interference} that was recently explored in a number of
publications~\cite{gaury_dynamical_2014, gaury_.c._2015}. The expressions
in ref.~\cite{gaury_dynamical_2014} for the number of particles transmitted
in a two-path Mach-Zehnder interferometer are applied to the present case:
\begin{equation}
\begin{split}
    n_\uparrow &= \frac{\bar{n}}{2} \qty[1 +
        \frac{1}{π}\sin(π\bar{n})\cos\qty(π\bar{n} + \frac{Δk_0}{2}\tilde{L})]\\
    n_\downarrow &= \frac{\bar{n}}{2} \qty[1 -
        \frac{1}{π}\sin(π\bar{n})\cos\qty(π\bar{n} + \frac{Δk_0}{2}\tilde{L})],
\end{split}
\end{equation}
where $\bar{n}$ is the number of particles injected by the voltage pulse,
$Δk_0$ is the difference between the wavevectors of the symmetric and
antisymmetric wavefunctions at the Fermi level, and $\tilde{L}$ is the length
over which the wires are coupled.

\begin{figure*}[tb]
    \centering
    \subcaptionbox{\captionstyle
		Output currents (red and green dashed lines) and input current (black)
		in the split wire as a function of time. \emph{Inset}: sketch of the
		simulated setup.
        }[0.49\linewidth]{\includegraphics[width=0.42\textwidth]{flying-qubit/I_t}}
    \hfill
    \subcaptionbox{\captionstyle
            Number of transmitted particles on the right in lead $\uparrow$
            ($n_\uparrow$) and lead $\downarrow$ ($n_\downarrow$) as a function
            of the injected number of particles ($\bar{n}$). Symbols:
            time-resolved simulation, dashed line: analytical result.
        }[0.49\linewidth]{\includegraphics[width=0.42\textwidth]{flying-qubit/nt_nbar}}
    \scaption{
        Charge transport after application of a voltage pulse on one
        lead of the split wire setup (shown as an inset in subfigure (a)).
    }
    \label{fig:sum:fly}
\end{figure*}


\subsection*{\color{ctcolorblack}Chapter \ref{ch:jj}:
             \color{ctcolorsection} Time-resolved Dynamics of Josephson Junctions}
A Josephson junction with a voltage bias applied between the superconducting
contacts is an inherently time-dependent system, which is illustrated by the
appearance of the a.c.\ Josephson effect.
%
This chapter deals with the dynamics of Josephson junctions with a static or
time-varying bias voltage applied. Because of the large separation of energy
scales required to study such a system in an experimentally
relevant regime (the superconducting gap must be very small compared with the
Fermi energy), the difference in time scales will be correspondingly large.
This requirement to simulate to very long times (compared to the smallest
time scale of the problem) is perfect for the source-sink algorithm due to
the linear scaling of the latter.

After an introduction to the relevant parts of the theory of conventional
superconductivity (Bogoliubov-de Gennes equation and Andreev reflection), we
start by studying the multiple Andreev reflection (MAR) processes responsible
for the finite current at voltages smaller than the superconducting gap.
Despite the fact that we are using a time-resolved approach to study an
essentially periodic problem, we nevertheless find \emph{quantitative
agreement} with theoretical results obtained using Floquet theory.

We then use the power of the time-resolved approach to study trains of voltage
pulses propagating in long Josephson junctions. We see that a \emph{periodic current},
is generated at the output even when just a
single voltage pulse is applied, as can be seen in \cref{fig:sum:jj}. The voltage
pulse generates an excitation in the junction that becomes trapped, as the pulse
is brief enough that the voltage is once again zero by the time the excitation
traverses the junction and returns (after an Andreev reflection) to the contact
where the pulse was applied.

We finally turn to short junctions, where the time of flight across the
junction is much shorter than the duration of the voltage pulse, and see that
we can still obtain a periodic current after a single voltage pulse. The pulse
creates an excitation in a superposition of the pair of Andreev bound states in
the junction, at energies $E$ and $-E$, which gives rise to a current
oscillating at frequency $2E/ℏ$.

\begin{figure*}[tb]
    \centering
    \includegraphics[width=0.6\textwidth]{josephson/DAR_current}
    \scaption{
        Current (blue full line) and voltage (red dashed line, offset for
        clarity) at the left superconducting-normal contact as a function of
        time. Inset: propagation of the charge pulse through the junction at
        different times ($t_1$, $t_2$, $t_3$, $t_4$) and the corresponding
        times indicated on the main plot.
    }
    \label{fig:sum:jj}
\end{figure*}

\subsection*{\color{ctcolorblack}Chapter \ref{ch:maj}:
             \color{ctcolorsection} Manipulating Andreev and Majorana Resonances in Nanowires}
The final application melds the concepts of interferometry introduced in
\cref{ch:fly} with those of superconductivity and Andreev bound states
introduced in \cref{ch:jj}. We study a system consisting of a nanowire coupled
to a superconductor, which have recently become a hot topic due to the presence
of a Majorana state, of which a signature is a peak in the conductance at
zero bias.

We start by treating the system in absence of the Majorana state, and show
that by applying a \emph{train of voltage pulses} we can manipulate the
peaks in the differential conductance that are present at voltage below the
superconducting gap due to the presence of Andreev resonances. In particular
we can shift the resonances to different voltages when applying trains of
different frequency.

Next we add further ingredients to the model (Rashba coupling and Zeeman
coupling), and work in a parameter regime where spin-momentum locking is
present, which gives rise to a Majorana state and a characteristic peak in the
differential conductance at zero bias. We show that the same technique using a
train of voltage pulses can be used to manipulate the Majorana resonance in the
same way.  We explore the effect of this train of pulses when the pulse
amplitude and frequency are changed, and even use this to perform
\enquote{spectroscopy} of the Majorana state, as illustrated in
\cref{fig:sum:maj}.  This reveals distinct signatures for the resonant Andreev
reflection mechanism that gives rise to the Majorana state.

This could be used as an \emph{extra experimental probe} to show that a
zero-bias conductance peak originates from resonant Andreev reflection, which
could provide evidence of its Majorana character.

\begin{figure}[tb]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth]{majorana/omega_E_colorplot}
        \scaption{Differential conductance in the presence of a sinusoidal
            voltage with $\bar{n}=0.5$ as a function of frequency and bias
            voltage.
        }
        \label{fig:maj:omega_E_colorplot}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth]{majorana/omega_nbar_colorplot}
        \scaption{Differential conductance at zero bias in the presence of a
            sinusoidal voltage pulse as a function of frequency and $\bar{n}$.
        }
        \label{fig:maj:omega_nbar_colorplot}
    \end{subfigure}
    \scaption{Differential conductance in the presence of a train
        of voltage pulses for a normal-insulator-normal-superconductor
        junction that displays a Majorana resonance in d.c.\ .
    }
    \label{fig:sum:maj}
\end{figure}

% reset
\addtocontents{toc}{\protect\setcounter{tocdepth}{\tocdepth}}
\renewcommand{\thefigure}{\arabic{chapter}.\arabic{figure}}
