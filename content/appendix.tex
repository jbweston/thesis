% -*- coding: utf-8 -*-
%
\def \imgpath {images/appendix/}
\graphicspath{{\imgpath}}

\chapter{Discretising Continuous models}
\label{sec:app:discretising}
This thesis deals primarily with performing numerical simulations of tight
binding models. In several instances the model that we actually want to
simulate is a continuous one, and a tight binding model is obtained by
a discretisation procedure. In this section we shall show how in practice
one obtains a discrete tight binding Hamiltonian from a continuous one.

We shall start from a general Hamiltonian of the form:
\begin{equation}
    \label{eq:app:hamiltonian-second-quantisation}
    \qop{\ham} = ∫_Ω \dd{\vb{x}} \ccreate{\vb{x}}\, H\qty(\vb{x}, iℏ∇, t)\, \cdestroy{\vb{x}}
\end{equation}
where $\ccreate{\vb{x}}$ ($\cdestroy{\vb{x}}$) is a fermionic creation operator
for a particle at position $\vb{x}$, $\ham\qty(\vb{x}, iℏ∇, t)$ is a differential
operator -- the realspace projection of the Hamiltonian -- and $Ω$ is the
domain of the problem.

First we shall approximate the integral using a simple rectangle method over a
uniform grid. \Cref{eq:app:hamiltonian-second-quantisation} is thus approximated by
\begin{equation}
    \label{eq:approximate-hamiltonian}
    \qop{\ham} \approx ∑_{i} \ccreate{\vb{x}_{i}}\, H\qty(\vb{x}_i, iℏ∇, t)\, \cdestroy{\vb{x}_{i}}.
\end{equation}
For $Ω=\reals^3$ we have $\vb{x}_i = a(n_i\vb{i} + m_i\vb{j} + l_i\vb{k})$,
with $a$ the discretisation step, $n_i,m_i,l_i \in \integers$, and $\vb{i}$,
$\vb{j}$ and $\vb{k}$ are unit vectors in the x, y, and z directions
respectively.  For other $Ω$ the allowed values of the $n_i,m_i,l_i$ should be
adjusted accordingly.

\section{Finite Difference Formulae}
\label{sec:app:fd}
Now we must approximate $\ham\qty(\vb{x}, iℏ∇, t)$, which we shall do using
central finite-difference formulae. All the Hamiltonians dealt with in this
thesis will be either linear or quadratic in $iℏ∇$, so it will be enough to
write down finite difference formulae for $∇$ and $∇^2$.  We define $δ_a$, the
finite difference operator with finite step $a$, in the following manner:
\begin{equation}
\label{eq:app:finite-differences}
    ∇\,{f(\vb{x})} \approx δ_a\qty[\,f](x) \equiv
      \frac{1}{2a} ∑_{\vb{n}}
      \qty[\, f(\vb{x} + a\vb{n}) - f(\vb{x} - a\vb{n}) ]\,\vb{n},
\end{equation}
where the sum runs over $\vb{n}\in\{\vb{i},\vb{j},\vb{k}\}$.  While this
expression has been written for three dimensions the equivalent expressions for
one and two dimensions are obvious.  We can obtain the finite difference
operator for second derivatives by applying \cref{eq:app:finite-differences}
twice:
\begin{equation}
\label{eq:app:second-finite-differences}
\begin{split}
    ∇^2\,{f(\vb{x})} \approx δ^2_a\qty[\,f](x) \equiv
        \frac{1}{a^2} \qty[\,
      - 6f(\vb{x})
      + ∑_{\vb{n}}
            f(\vb{x} + a\vb{n}) + f(\vb{x} - a\vb{n})
        ].
\end{split}
\end{equation}
The factor of $6$ in the term proportional to $f(\vb{x}))$ is particular to 3D;
in general there is a term $-2f(\vb{x})$ for each dimension.  In evaluating
these formulae we see that there are two types of term that appear:
\enquote{onsite} terms, proportional to $f(\vb{x})$, and \enquote{hopping}
terms proportional to $f(\vb{x} \pm a\vb{n})$.  Note that in evaluating the
formula for the second derivative we twice-applied the finite difference
operator with discretisation spacing $a/2$, which in the end produced a scheme
that only contains function evaluations at points spaced by $a$. This is
advantageous for treating Hamiltonians that contain both first and second
derivatives, as both only give first-nearest neighbour terms.

The above finite difference formulae are accurate to $\order{a^2}$ and only
contain nearest-neighbour terms, but we could also define higher-order formulae
accurate to $\order{a^4}$ or $\order{a^6}$ at the expense of having to include
second and third nearest-neighbour terms, which renders the tight-binding
Hamiltonian matrix less sparse. In this thesis we use the above
$\order{a^2}$ accurate formulae exclusively.


\section{From Continuum to Tight-Binding Models}
If we apply the central difference formulae to the $\cdestroy{\vb{x}_i}$ of
\cref{eq:approximate-hamiltonian} we can see that we are going to generate
terms at neighbouring points $\cdestroy{\vb{x}_j}$. The Hamiltonian will thus
mix $\ccreate{\vb{x}_i}$ and $\cdestroy{\vb{x}_j}$ at neighbouring points.  The
most general form we can write down for \cref{eq:approximate-hamiltonian} is
then
\begin{equation}
    \label{eq:app:tb-local}
    ∑_{ij} \ham_{ij} \, \ccreate{\vb{x}_i} \, \cdestroy{\vb{x}_j},
\end{equation}
where the $H_{ij}$ contains the coefficients from the finite difference
formulae that bring the point $\vb{x}_j$ to $\vb{x}_i$.  We recognise
\cref{eq:app:tb-local} as a tight-binding Hamiltonian in second quantisation.

Up till now this may seem rather abstract, so let us apply this to a concrete
example: a particle in 1D in the presence of a potential landscape.
We write the continuum Hamiltonian as
\begin{equation}
    \qop{\ham} = ∫_{-\infty}^{\infty} \dd{x} \ccreate{x}\,
                 \qty[\frac{-ℏ^2}{2m^2}\pdv[2]{x} + V(x, t)]\,
                 \cdestroy{x}
\end{equation}
and discretise onto the lattice of points $\qty{x_n = na \;|\; n\in\integers}$
using the above procedure to obtain
\begin{equation}
    \qop{\ham} \approx ∑_n \ccreate{x_n}\qty[\frac{-ℏ^2}{2m^*a^2}\qty(
        \cdestroy{x_{n+1}} + \cdestroy{x_{n-1}} - 2\cdestroy{x_n})
        + V(x_n, t)\cdestroy{x_n}].
\end{equation}
Rearranging terms we get
\begin{equation}\begin{split}
    \qop{\ham} \approx \, ∑_n \Bigg[
    &\qty(\frac{ℏ^2}{m^*a^2} + V(x_n, t))\ccreate{x_n}\cdestroy{x_n}\\
    &+ \frac{-ℏ^2}{2m^*a^2}\ccreate{x_n}\cdestroy{x_{n+1}}
    + \frac{-ℏ^2}{2m^*a^2}\ccreate{x_n}\cdestroy{x_{n-1}}
    \Bigg],
\end{split}\end{equation}
which allows us to identify the coefficients $\ham_{ij}$ of the tight-binding
model:
\begin{equation}\begin{split}
    \ham_{ii} &= \frac{ℏ^2}{m^*a^2} + V(x_i, t),\\
    \ham_{i\,i+1} &= -\frac{ℏ^2}{2m^*a^2},
\end{split}\end{equation}
and
\begin{equation}
    \ham_{i\,i-1} = -\frac{ℏ^2}{2m^*a^2},
\end{equation}
with all other terms 0.



\chapter{The Peierls Substitution}
\label{sec:app:peierls-nomag}
Here we shall derive the so-called Peierls substitution~\cite{peierls_zur_1933,
graf_electromagnetic_1995} that is commonly used to introduce a vector
potential into tight-binding models. Contrary to most presentations in the
literature we shall start from a continuum Hamiltonian, as all the models
presented in this thesis start from such a description and arrive at a
tight-binding Hamiltonian only after discretisation using the procedure outlined
in \cref{sec:app:fd}.

\section{Peierls Substitution Without Magnetic Field}
We shall first treat the case
when there is no magnetic field: $\curl{\vb{A}} = 0$. While this might seem
contrived, we often treat cases where a time-dependent (but spatially uniform)
voltage is applied to the leads of a nanoelectronic device. It is very useful
to be able to express such a system in a gauge where there is a time-dependent
vector potential \emph{at the voltage drop}, as opposed to a time-dependent
scalar potential everywhere in the lead. In this gauge the electric field is
given by $\vb{E} = -∂\vb{A}/∂t$. This treatment allows us to identify
the formal unitary gauge transformation presented in \cref{sec:app:gauge-lead} as
the discrete analogue of the continuum \emph{electromagnetic} gauge transformation
presented here.

Let us start with a minimally-coupled Hamiltonian in first quantisation of
the form
\begin{equation}
    H\qty(\vb x, iℏ∇) = \frac{1}{2m}\qty[ -iℏ∇ + q\vb{A}(\vb{x}) ]^2.
\end{equation}
Although we could try to discretise this Hamiltonian directly using
the approach demonstrated in \cref{sec:app:discretising}, for the
present case with no magnetic field it is actually simpler to first
recast the Hamiltonian into a different form.
Consider the action of the canonical
momentum operator $\vb{Π}$ on a function $f(\vb{x})$:
\begin{equation}
    \vb{Π}\,f(\vb{x}) \equiv \qty[ -iℏ∇ + q\vb{A}(\vb{x}) ]\,f(\vb{x}).
\end{equation}
Compare this to the action of the operator $\vb{Q}$ on $f(\vb{x})$:
\begin{equation}
    \vb{Q}\,f(\vb{x}) \equiv
    -iℏ e^{-i(q/ℏ)∫_{\vb{0}}^{\vb{x}} \vb{A}(\vb{x}') \cdot\, \dd{\vb{x}'}}
    ∇ \qty(
        e^{i(q/ℏ)∫_{\vb{0}}^{\vb{x}} \vb{A}(\vb{x}') \cdot\, \dd{\vb{x}'}}
        f(\vb{x})
    ),
\end{equation}
%
which we can evaluate explicitly -- using the fact that $∫_{\vb 0}^{\vb x} \vb{A}(\vb{x}', t) \cdot
\dd{\vb{x}'}$ is path independent ($\curl{\vb{A}} = 0$) and hence a
well-defined function of $\vb{x}$ -- to obtain
%
\begin{equation}
  \vb{Q}\,f(\vb{x}) = [-iℏ∇ + q\vb{A}(\vb{x})]\,f(\vb{x}).
\end{equation}
%
We thus see that $\vb{Q} = \vb{Π}$. We
see that we can thus write the action of the Hamiltonian on $f(\vb{x})$ as
\begin{equation}
    H\qty(\vb{x}, iℏ∇)\,f(\vb{x}) = \frac{-ℏ^2}{2m}
        e^{-i(q/ℏ)∫_{\vb{0}}^{\vb{x}} \vb{A}(\vb{x}') \cdot\, \dd{\vb{x}'}}
        ∇^2\qty(
            e^{i(q/ℏ)∫_{\vb{0}}^{\vb{x}} \vb{A}(\vb{x}') \cdot\, \dd{\vb{x}'}}
            f(\vb{x})
        ).
\end{equation}
Using \cref{eq:app:second-finite-differences} we can write a discretised
approximation for $H\qty(\vb{x}, iℏ∇)\,f(\vb{x}) $:
\begin{equation}
\label{eq:app:tb-vector-potential}
\begin{split}
    H\qty(\vb{x}, iℏ∇)\,f(\vb{x}) \approx \frac{-ℏ^2}{2ma^2}\qty[
        - 6f(\vb{x})
        + ∑_{\vb{n}}
            e^{iφ_{\vb x}(  a\vb{n})}f(\vb{x} + a\vb{n}) +
            e^{iφ_{\vb x}(- a\vb{n})}f(\vb{x} - a\vb{n})
    ]
\end{split}
\end{equation}
where $φ_{\vb x}(\vb{y}) = (q/ℏ)∫_{\vb x}^{\vb{x} + \vb{y}}
\vb{A}(\vb{x}')\cdot\dd{\vb{x}'}$.  We see that
\cref{eq:app:tb-vector-potential} is the same as the tight binding model in the
absence of $\vb{A}$,  except that the hopping terms have picked up phase
factors.

\section{Peierls Substitution With Magnetic Field}
Let us now treat the case \emph{with} magnetic field. Due to the fact that
$∫_{\vb{0}}^{\vb{x}} \vb{A}(\vb{x}') \cdot\, \dd{\vb{x}'}$ is now path
dependent we cannot use the same formal reasoning as before. In the end, however,
we will end up with the same general form for the discretised model, but this
time it will be an \emph{approximation} that is only valid when the magnetic field
varies slowly on the length scale $a$ of the discretisation.
We start as before from the minimally coupled Hamiltonian, however this time
we explicitly expand out the terms:
\begin{equation}
    H(\vb{x}, iℏ∇)\,f(\vb{x}) =
    \frac{-ℏ^2}{2m}∇^2f(\vb{x}) -
    \frac{iℏq}{2m} \qty[
        f(\vb{x})\,∇\cdot\vb{A}(\vb{x}) +
        2\vb{A}(\vb{x})\cdot∇f(\vb{x})
    ] +
    q^2 \vb{A}(\vb{x}) \cdot \vb{A}(\vb{x}).
\end{equation}
We now discretise this using the techniques of \cref{sec:app:fd}
onto a cubic lattice with spacing $a$:
\begin{equation}
\begin{split}
    H(\vb{x}, iℏ∇)\,f(\vb{x}) \approx&
    - ∑_{\vb{n}} \qty[
        \frac{ℏ^2}{2ma^2} + \frac{iℏq}{2ma}\vb{A}(\vb{x})\cdot\vb{n}
    ]\,f(\vb{x}+a\vb{n})\\
    %
    &- ∑_{\vb{n}} \qty[
        \frac{ℏ^2}{2ma^2} - \frac{iℏq}{2ma}\vb{A}(\vb{x})\cdot\vb{n}
    ]\,f(\vb{x}-a\vb{n})\\
    %
    &+ \qty[
        \frac{3ℏ^2}{ma^2} +
        q^2\abs{\vb{A}(\vb{x})}^2 -
        \frac{iℏq}{4ma}∑_{\vb{n}} \qty[\vb{A}(\vb{x}+a\vb{n}) - \vb{A}(\vb{x}-a\vb{n})]\cdot\vb{n}
    ]\,f(\vb{x}).
\end{split}
\end{equation}
Now we will re-express the above in units of $γ=ℏ^2/2ma^2$ in order to more easily
see the relative order in powers of $a$ of the different terms:
\begin{equation}
\begin{split}
    H(\vb{x}, iℏ∇)\,f(\vb{x}) \approx&
    - γ∑_{\vb{n}} \qty[
        1 + \frac{iqa}{ℏ}\vb{A}(\vb{x})\cdot\vb{n}
    ]\,f(\vb{x}+a\vb{n})\\
    %
    &- γ∑_{\vb{n}} \qty[
        1 - \frac{iqa}{ℏ}\vb{A}(\vb{x})\cdot\vb{n}
    ]\,f(\vb{x}-a\vb{n})\\
    %
    &+ γ\qty[
        6 +
        \frac{2ma^2q^2}{ℏ^2} \abs{\vb{A}(\vb{x})}^2 -
        \frac{iqa}{2ℏ}∑_{\vb{n}} \qty[\vb{A}(\vb{x}+a\vb{n}) - \vb{A}(\vb{x}-a\vb{n})]\cdot\vb{n}
    ]\,f(\vb{x}).
\end{split}
\end{equation}
Now we will make some approximations in order to proceed. The first
approximation will be that the magnetic field is constant over
the length scale $a$.  The consequence of this is that $\vb{A}(\vb{x})$
varies \emph{linearly} over a distance $a$, and $\vb{A}(\vb{x}+a\vb{n}) - \vb{A}(\vb{x}-a\vb{n}) \propto 2a$.
Next, in order to be compatible with the $\order{a^2}$ approximations to the first and second
derivatives, we should discard all terms of $\order{a^2}$. This leaves us with
\begin{equation}
\label{eq:app:nearly-peierls}
\begin{split}
    H(\vb{x}, iℏ∇)\,f(\vb{x}) \approx&
    - γ∑_{\vb{n}} \qty[
        1 + \frac{iqa}{ℏ}\vb{A}(\vb{x})\cdot\vb{n}
    ]\,f(\vb{x}+a\vb{n})\\
    %
    &- γ∑_{\vb{n}} \qty[
        1 - \frac{iqa}{ℏ}\vb{A}(\vb{x})\cdot\vb{n}
    ]\,f(\vb{x}-a\vb{n})\\
    %
    &+ 6γ\,f(\vb{x}).
\end{split}
\end{equation}
The final part is to notice that $e^{ix} = 1 + ix + \order{x^2}$, and since we
have discarded all terms of $\order{a^2}$ we can write
\cref{eq:app:nearly-peierls} as:
\begin{equation}
    H(\vb{x}, iℏ∇)\,f(\vb{x}) \approx \frac{-ℏ^2}{2ma^2}\qty[
        6f(\vb{x}) + ∑_{\vb{n}}
            e^{i\tilde{φ}_{\vb{x}}(a\vb{n})}f(\vb{x}+a\vb{n}) +
            e^{-i\tilde{φ}_{\vb{x}}(a\vb{n})}f(\vb{x}-a\vb{n})
    ],
\end{equation}
with $\tilde{φ}_{\vb{x}}(\vb{y}) = (q/ℏ) \vb{A}(\vb{x})\cdot\vb{y}$.
We see that this is the same expression as \cref{eq:app:tb-vector-potential},
except that $\tilde{φ}_{\vb{x}}(a\vb{n})$ has replaced $φ_{\vb{x}}(a\vb{n})$.
The Peierls substitution is therefore also valid when there is a magnetic
field, except in this case it is an \emph{approximation} that is valid
when the magnetic field is roughly constant on length scales $a$.


\chapter{Gauge Transformations}
\label{sec:app:gauge}
In this section we shall look at some important gauge transformations
that are used in this thesis to bring a problem with time dependence
in the infinite \emph{leads} of a nanoelectronic system to a problem
with time dependence only in a finite region.

First we recall the expression for a general, time-dependent gauge
transformation on an arbitrary Hamiltonian. We start from the
Schrödinger equation
\begin{equation}
    \label{eq:app:schrodinger}
    iℏ\pdv{t} \ket{ψ(t)} = \qop{\ham}(t) \ket{ψ(t)}.
\end{equation}
and define
\begin{equation}
    \label{eq:app:gauge}
    \ket{ψ(t)} = \qop{U}(t)\ket{ψ'(t)},
\end{equation}
where $\qop{U}(t)$ is a unitary operator (the gauge transformation
of interest), and plug~\ref{eq:app:gauge}
into~\ref{eq:app:schrodinger}:
\begin{equation}
    iℏ\qty(\pdv{t} \qop{U}(t)) \ket{ψ'(t)} +
    iℏ\qop{U}(t) \pdv{t} \ket{ψ'(t)} =
    \qop{\ham}(t) \qop{U}(t) \ket{ψ'(t)}.
\end{equation}
Now we multiply on the left by $\cqop{U}(t)$ to obtain
\begin{equation}
    \label{eq:app:new-schrodinger}
    iℏ\pdv{t}\ket{ψ'(t)} = \qty(
        \cqop{U}(t) \qop{\ham}(t) \qop{U}(t) -
        iℏ\cqop{U}(t) \pdv{t} \qop{U}(t)
        ) \ket{ψ'(t)},
\end{equation}
which we recognise as a Schrödinger equation for $\ket{ψ'(t)}$ with
a modified Hamiltonian
\begin{equation}
    \label{eq:app:new-hamiltonian}
    \qop{\ham}'(t) =
        \cqop{U}(t) \qop{\ham}(t) \qop{U}(t) -
        iℏ\cqop{U}(t) \pdv{t} \qop{U}(t).
\end{equation}
We thus conclude that \cref{eq:app:schrodinger} and \cref{eq:app:new-schrodinger}
represent the same physical situation, and \cref{eq:app:new-hamiltonian} is
the transformed Hamiltonian subject to a time-dependent gauge transformation
$\qop{U}(t)$.


\section{Gauge Transformations in Semi-Infinite Leads}
\label{sec:app:gauge-lead}
Several times in the main text we make use of the fact that systems that we
consider have infinite, periodic leads that are time invariant.  Here we shall
show that if we start from a system where a lead  has a uniform, time-varying
potential applied to it we can perform a gauge transformation to remove the
time dependence from the lead and bring it into the coupling between the lead
and the scattering region.  We shall treat a class of systems consisting of a finite,
scattering region, $S$, with an arbitrary quadratic Hamiltonian coupled to a
semi-infinite electrode, $L$, with a uniform but time-dependent voltage $V(t)$
applied to it.  The Hamiltonian is written as
\begin{equation}
    \label{eq:app:gauge-hamiltonian}
    \qop{\ham}(t) =
    \underbrace{∑_{ij} \ham^S_{ij}(t) \cqop{c}_i \qop{c}_j}
               _{\qop{\ham}^S(t)} \;
  + \underbrace{∑_{ij} \ham^T_{ij}(t) \cqop{c}_i \qop{d}_j + h.c.}
               _{\qop{\ham}^T(t)}\;
  + \underbrace{∑_{ij} \qty( \ham^L_{ij} + V(t)δ_{ij}) \cqop{d}_i \qop{d}_j}
               _{\qop{\ham}^L(t)}\;
\end{equation}
where $\create_i$ ($\destroy_j$) are the creation (annihilation) operators
for fermions in the scattering region, and $\cqop{d}_i$ ($\qop{d}_j$)
are the corresponding operators in the leads. Note that, as mentioned above,
the time-dependence in the lead is given entirely by $V(t)$. We now choose to
apply a gauge transformation
\begin{equation}
    \label{eq:app:gauge-transform}
    \qop{W}(t) = ∏_{i} \exp[-(i/ℏ)φ(t)\cqop{d}_i \qop{d}_i],
\end{equation}
where
\begin{equation}
    \label{eq:app:phi}
    φ(t) = ∫ V(t)\,\dd{t}.
\end{equation}
We shall now use \cref{eq:app:new-hamiltonian} with \cref{eq:app:gauge-transform,eq:app:gauge-hamiltonian}
to obtain the transformed Hamiltonian. First evaluating $\cqop{W}(t) \qop{\ham}(t)\qop{W}(t)$,
term by term we note that $\commutator{\qop{W}(t)}{\qop{\ham}^S(t)} = 0$ because
$\commutator{\cqop{c}_i\qop{c}_j}{\cqop{d}_k\qop{d}_k} = 0$. Similar reasoning leads
us to $\commutator{\qop{W}(t)}{\qop{\ham}^L(t)} = 0$. The $\cqop{W}(t) \qop{\ham}^T(t)\qop{W}(t)$
term requires the use of the following operator identities:
\begin{align}
    \label{eq:app:operator-identities}
    e^{iα\cqop{g}\qop{g}} \, \cqop{g} \, e^{-iα\cqop{g}\qop{g}} &= e^{iα}\cqop{g}\\
    e^{iα\cqop{g}\qop{g}} \, \qop{g} \, e^{-iα\cqop{g}\qop{g}} &= e^{-iα}\qop{g},
\end{align}
where $α$ is a complex number and $\qop{g}$ is a fermionic operator (satisfying
anticommutation relation $\acomm{\qop{g}, \cqop{g}} = 1$, and yields
\begin{equation}
    \cqop{W}(t) \qop{\ham}^T(t)\qop{W}(t) = ∑_{ij} \ham^T_{ij}(t)e^{-iφ(t)}\cqop{c}_i\qop{d}_j + h.c.
\end{equation}
Now evaluating $-iℏ\cqop{W}(t)\pdv{t}\qop{W}(t)$ we get
\begin{equation}
    -iℏ\cqop{W}(t)\pdv{t}\qop{W}(t) = - V(t) ∑_{ii} \cqop{d}_i\qop{d}_i.
\end{equation}
Putting this all together allows us to write the transformed Hamiltonian as
\begin{equation}
    \label{eq:app:transformed-ham}
    \qop{\ham}'(t) =
    ∑_{ij} \ham_{ij}^S(t) \cqop{c}_i \qop{c}_j \;
    + ∑_{ij} \ham_{ij}^T(t)e^{-iφ(t)} \cqop{c}_i \qop{d}_j + h.c. \;
  + ∑_{ij} \ham^L_{ij} \cqop{d}_i \qop{d}_j,
\end{equation}
where we see that the time dependence has been moved from the leads into the
lead-system coupling term.
This calculation generalises trivially to the case where there are several
semi-infinite electrodes, each with their own uniform, time-dependent voltage.
In addition it is interesting to note that this
gauge transformation coincides with that of \cref{sec:app:peierls-nomag}
if our tight-binding model is the discretisation of a continuum model.  This
gauge transformation is, in effect, a transformation from the Coulomb gauge
($\divergence{\vb{A}} = 0$) to the Lorentz gauge ($\divergence{\vb{A}} +
(1/c^2)∂φ/∂t = 0$).


\section{Gauge Transformations for Superconducting Leads}
Here we shall look at the case of a system with a superconducting lead
that has a time-dependent bias applied to it. We shall see that while
the problems look similar in the Lorentz gauge, in the Coulomb gauge
there is an extra time-dependence that appears in the anomalous terms
$\cqop{c}\cqop{c}$ in the Hamiltonian.

We shall start with the Hamiltonian of a system with a superconducting lead
that has a time-dependent bias applied to it. In the Lorentz gauge the
Hamiltonian reads:
\begin{equation}
    \label{eq:app:super-ham}
    \begin{split}
    \qop{\ham}'(t) =
    &∑_{ij} \qty(\ham_{ij}^S(t) - μδ_{ij}) \cqop{c}_i \qop{c}_j \;
    + ∑_{ij} \ham_{ij}^T(t)e^{-iφ(t)} \cqop{c}_i \qop{d}_j + h.c.\;
    +\\ &∑_{ij} \qty(\ham^L_{ij} - μδ_{ij}) \cqop{d}_i \qop{d}_j + Δ_{ij}\cqop{d}_i\cqop{d}_j + h.c.,
    \end{split}
\end{equation}
with $φ(t)$ defined by \cref{eq:app:phi}.  This is just
\cref{eq:app:transformed-ham} with the Fermi level $μ$ subtracted and an extra
anomalous term (see \cref{sec:jj:bdg} for details).  We shall
now apply the gauge transformation
\begin{equation}
    \qop{W}(t) = ∏_{i} \exp[(i/ℏ)φ(t)\cqop{d}_i \qop{d}_i],
\end{equation}
(the inverse of \cref{eq:app:gauge-transform}) to bring us into the Coulomb
gauge. The treatment is identical to \cref{sec:app:gauge-lead}, except that now
we have to evaluate terms of the form
\begin{equation}
    Δ_{ij}\cqop{W}(t)\cqop{d}_i\cqop{d}_j\qop{W}(t).
\end{equation}
Using \cref{eq:app:operator-identities} we see that
\begin{equation}
    Δ_{ij}\cqop{W}(t)\cqop{d}_j\cqop{d}_j\qop{W}(t) = Δ_{ij}e^{2iφ(t)}\cqop{d}_i\cqop{d}_j,
\end{equation}
i.e. the anomalous terms responsible for superconductivity pick up time-varying
phase factors. The Hamiltonian in the Coulomb gauge, $\qop{\ham}(t)$, is then
\begin{equation}
    \label{eq:app:super-ham-coulomb}
    \begin{split}
    \qop{\ham}(t) =
    &∑_{ij} \qty(\ham_{ij}^S(t) - μδ_{ij}) \cqop{c}_i \qop{c}_j \;
    + ∑_{ij} \ham_{ij}^T(t)e^{-iφ(t)} \cqop{c}_i \qop{d}_j + h.c.\;
    +\\ &∑_{ij} \qty(\ham^L_{ij} - [μ - V(t)]δ_{ij}) \cqop{d}_i \qop{d}_j
    + Δ_{ij}e^{2iφ(t)}\cqop{d}_i\cqop{d}_j + h.c.
    \end{split}
\end{equation}
It is now clear that even if the bias voltage is constant the Hamiltonian for
treating this system will always be time dependent due to the phase factors --
this is true regardless of the gauge in which we try to treat the problem.
This inherent time dependence is what gives rise to the a.c.\ Josephson effect.
