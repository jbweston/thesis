% -*- coding: utf-8 -*-
%
\def \imgpath {images/}
\graphicspath{{\imgpath}}
\renewcommand{\thefigure}{\arabic{figure}}
\addcontentsline{toc}{chapter}{Résumé de la Thèse (french)}
\graphicspath{{\imgpath}}

\begin{otherlanguage}{french}
\chapter*{Résumé de la Thèse (français)}
\addtocontents{toc}{\protect\setcounter{tocdepth}{0}}
\label{ch:sum-fr}

Au sein de ces pages on traite le problème de transport d'électrons au sein
de dispositifs quantiques lorsqu'ils subissent des perturbations dépendantes
du temps, par exemple des impulsions de tensions appliquées à des contactes
ou à des grilles, de la radiation électromagnétique, etc.
Cette thèse consiste en deux parties principales.  La
première partie est dédiée aux avancées sur les techniques théoriques ainsi
que leur implémentation numérique qui nous
permettent de simuler un système en un temps qui croît linéairement avec
le volume du système ainsi que le temps maximal (de simulation) requis.
Ceci représente une amélioration par rapport à l'état de l'art, où la durée
d'une simulation croît avec le temps maximale requis \emph{au carré}.
Au cours de la deuxième partie on applique ces algorithmes à plusieurs
systèmes: un interféromètre à qubits volants, une jonction Josephson sous
tension, et un nanofil couplé à un supraconducteur, qui produit des états
de Majorana à ses extrémités. Dans la suite de ce résumé, on présente
brièvement les résultats clés de chaque chapitre.


\subsection*{\color{ctcolorblack}Chapitre \ref{ch:qt}:
             \color{ctcolormain} Introduction au Transport Quantique Résolu en Temps}
On commence en introduisant le transport quantique de manière générale; en
particulier on regarde les longueurs caractéristiques, ainsi qu'un
dispositif archétype du transport quantique: un gaz d'électrons
bidimensionnel à l'interface entre deux matériaux semi-conducteurs.  Après
on regarde en plus de détail de récents expériences sur des sources
d'électron unique, ce qui motive notre intérêt dans le transport
quantique résolu en temps. On suit avec un bref examen des techniques
numériques sur le marché actuellement: les techniques de fonctions de Green
hors équilibre, et les techniques basées sur les fonctions d'onde. Ce
chapitre ne contient pas de résultat nouveau, mais sert d'introduction pour
la suite.


\subsection*{\color{ctcolorblack}Chapitre \ref{ch:num}:
             \color{ctcolormain} Algorithmes Numériques pour le Transport Quantique Résolu en Temps}
Dans ce chapitre on présente la façon dont les algorithmes ont été améliorées par rapport
à l'état de l'art. Le cœur de nôtre approche consiste à
calculer les états propres du système infini (cette partie est déjà connue
depuis longtemps), et puis les évoluer en temps avec l'équation de
Schrödinger dépendant du temps; les observables sont en suite calculés
en intégrant sur les contributions de ces états évolués dans le temps.
Enfin les équation différentielle à résoudre sont
\begin{equation}
\begin{split}
    i\pdv{t}\bar{ψ}_{αE}(t) &= [\mb{H}(t) - E]\bar{ψ}_{αE}(t) +
                              \underbrace{\mb{W}(t) ψ^{\mr{st}}_{αE}}_
                                         {\mr{terme\ de\ source}} -
                              \underbrace{i\mb{Σ} \bar{ψ}_{αE}(t)}_
                                         {\mr{terme\ de\ fuite}}
    \\
    ψ_{αE}(t) &= \qty[\bar{ψ}_{αE}(t) + ψ^{\mr{st}}_{αE}]e^{-iEt},
\end{split}
\end{equation}
où $ψ_{αE}(t)$ est la fonction d'onde qu'on souhaite calculer, $ψ^{\mr{st}}_{αE}$
est l'état de \enquote{scattering} initial associé au mode $α$ et énergie $E$,
$\mb{H}(t)$ est la matrice du Hamiltonien, $\mb{W}(t) = \mb{H}(t) - \mb{H}(0)$,
et $\mb{Σ}$ est une matrice diagonale qui est non-nulle sur un nombre fini de
cellules des contactes infinis périodiques qui sont attachés au système centrale.

%
La différence clé par rapport aux approches précédentes est qu'on se sert d'un
\emph{potentiel complexe absorbant}, $-i\mb{Σ}$,  pour traiter les conditions limites à
l'interface entre le système centrale et les électrodes. Ceci nous fournit
une algorithme dont le temps d'exécution croît linéairement avec le volume
du système centrale et le temps (de simulation) requis. Les termes \enquote{source}
et \enquote{sink} présent dans l'équation Schrödinger nous fournissent le nom
\enquote{source-sink} pour cette méthode.

En plus de ce
dernier, on calcule la coefficient de réflexion pour une forme arbitraire du
potentiel absorbant, en allant au delà de l'approximation WKB. On trouve que
la réflexion du potentiel complexe satisfait
\begin{equation}
\begin{split}
    r_Σ(E) = i&e^{2ikL}e^{-Ak/E} +\\
    &\frac{1}{4iEL}∫_0^\infty Σ'(u) \exp\qty{2ikLu - \frac{k}{E}∫_0^u Σ(v) \dd{v}} \dd{u}
    + \order{\qty(\frac{1}{kL})^2},
\end{split}
\end{equation}
où $Σ(x)$ est le potentiel absorbant en fonction de la distance dans la région
absorbante, $L$ est la longueur de la région absorbante, et $k$ est le vecteur
d'onde qui correspond à une énergie $E$.

Finalement, on explique la façon dont les observables peuvent se calculer en
intégrant les contributions des fonctions d'onde, en pondérant par la
distribution de Fermi-Dirac appropriée. On propose, en revanche,
\emph{d'intégrer en quantité de mouvement}, plutôt qu'en énergie --- comme
font la majorité des autres méthodes du même type.  Ce
changement nous permet d'éviter des singularités qui sont présent chaque
fois qu'il y a une ouverture de mode dans les électrodes du système. Les
intégrations en quantité de mouvement nécessitent, donc, moins
d'évaluations de l'intégrande pour atteindre la même précision; on
illustre ce point avec deux exemples simples.


\subsection*{\color{ctcolorblack}Chapitre \ref{ch:soft}:
             \color{ctcolormain} Conception Logicielle}
Dans ce chapitre on discute des propriétés voulues d'un logiciel implémentant
les algorithmes présentées auparavant. On commence en motivant le besoin de
trouver les bonnes abstractions pour les concepts mathématiques de base, plutôt
que de créer un logiciel monolithique sans possibilité de s'adapter aux
diverses besoins des utilisateurs différents. On prend l'exemple du logiciel \kwant
et illustre --- par le biais d'un exemple simple concret mais simple --- comment
ce dernier implémente cette philosophie pour du transport quantique \emph{indépendant}
du temps. En particulier on souligne la façon dont \kwant permet aux utilisateurs
à exprimer le problème à résoudre \emph{en termes de la structure mathématique},
plutôt qu'en termes de sa représentation de bas niveau dont \emph{l'ordinateur}
a besoin pour résoudre le problème.

On suivra en précisant la fonctionnalité
supplémentaire qui devraient être rajoutée à \kwant pour le rendre capable de
traiter des problèmes qui dépendent du temps; on appelle cette nouvelle
fonctionnalité \tkwant de l'anglais \enquote{time-dependent \kwant}. On
termine ce chapitre avec une sélection d'exemples d'usage de la version
actuelle de \tkwant, en dehors du travail présenté dans cette thèse: un
calcul de bruit résolu en temps; arrêter des électrons dans la régime de
Hall quantique; un régime universel transitoire pour des impulsions de
tension appliquées à des interféromètres; et des isolants topologiques
de Floquet.  En particulier, \tkwant a commencé à être utilisé en dehors du
groupe de recherche de l'auteur ce qui indique que --- même dans son état
préliminaire --- le logiciel est capable de fournir de la vraie valeur à des
projets de recherche.


\subsection*{\color{ctcolorblack}Chapitre \ref{ch:fly}:
             \color{ctcolormain} Qubit Volant}
A ce point on commence la deuxième partie de la thèse, qui se concerne
des applications spécifiques des algorithmes développées auparavant.
La première système qu'on étudie est un \enquote{fil fendu} implémenté
dans un gaz d'électrons bidimensionnelle, qui consiste de deux régions quasi
unidimensionnelle séparées par une barrière à effet tunnel. Ce dispositif
a été proposé pour implémenter un \enquote{qubit volant}, où l'état d'un
électron est modifié lors de son trajet dans le circuit. Ce dispositif
est le sujet d'un étude expérimentale actuelle au sein du groupe de
recherche de Christopher Bäuerle à l'Institut Néel à Grenoble.

On commence en traitant le problème sans dépendance en temps, avec un
approche de type diffusion. Grâce à ce point de vu, on est capable
d'apprécier que ce système à une caractère d'interféromètre, où les
états symétriques et antisymétriques jouent le rôle des deux chemins
à travers le système. Par la suite, on applique
une \emph{impulsion de tension} à travers le système. On voit que le nombre
de charges récupéré à l'électrode de sortie \emph{oscille} en fonction du
nombre de charges envoyé par l'impulsion, ceci est montré en \cref{fig:sum_fr:fly}.
Expérimentalement ceci correspondrait à une mesure du courant moyen lorsque
l'impulsion de tension est répété dans le temps.  On interprète ce résultat dans
le cadre de modification dynamique du schéma d'interférence qui a été
récemment proposé. L'expression de la ref.~\cite{gaury_dynamical_2014} pour
le nombre de particules transmis dans un interféromètre à deux chemins
(Mach-Zehnder) est appliquée au cas présent:
\begin{equation}
\begin{split}
    n_\uparrow &= \frac{\bar{n}}{2} \qty[1 +
        \frac{1}{π}\sin(π\bar{n})\cos\qty(π\bar{n} + \frac{Δk_0}{2}\tilde{L})]\\
    n_\downarrow &= \frac{\bar{n}}{2} \qty[1 -
        \frac{1}{π}\sin(π\bar{n})\cos\qty(π\bar{n} + \frac{Δk_0}{2}\tilde{L})],
\end{split}
\end{equation}
où $\bar{n}$ est le nombre de particules injectés par l'impulsion de tension,
$Δk_0$ est la différence entre les vecteurs d'onde des fonctions d'onde symétriques
et antisymétriques au niveau de Fermi, et $\tilde{L}$ est la longueur sur laquelle
les fils sont couplés.

\begin{figure*}[tb]
    \centering
    \subcaptionbox{\captionstyle
		Courants de sortie (lignes en tireté rouges et vertes) et courant
		d'entré (ligne noire) dans le système \enquote{fil fendu} en fonction
		du temps. \emph{Cartouche}: esquisse du système simulé.
        }[0.49\linewidth]{\includegraphics[width=0.42\textwidth]{flying-qubit/I_t}}
    \hfill
    \subcaptionbox{\captionstyle
			Nombre de particules transmis à droite dans le contact $\uparrow$
			($n_\uparrow$) et le contact $\downarrow$ ($n_\downarrow$) en
			fonction du nombre de particules injectés  ($\bar{n}$). Symboles:
			simulations résolues en temps, ligne en tireté: résultat analytique.
        }[0.49\linewidth]{\includegraphics[width=0.42\textwidth]{flying-qubit/nt_nbar}}
    \scaption{
		Transport de charge après une impulsion de tension sur un des contactes
		du \enquote{fil fendu} (en cartouche (a)).
    }
    \label{fig:sum_fr:fly}
\end{figure*}


\subsection*{\color{ctcolorblack}Chapitre \ref{ch:jj}:
             \color{ctcolormain} Dynamique Résolue en Temps d'une Jonction Josephson}
Une jonction Josephson qui subit une tension entre ses contactes est un système
fondamentalement dépendant du temps, ce qui est bien illustré par la présence
de l'effet Josephson a.c..
%
Dans ce chapitre on traitera la dynamique d'une jonction Josephson qui subit
une tension soit statique soit qui varie dans le temps. En raison de la grande
séparation d'échelles d'énergies nécessaire pour étudier un tel système dans un
régime qui est relevant expérimentalement (le gap supraconducteur doit être
petit par rapport au niveau de Fermi), la différence entre les échelles de
temps doit être grande de la même façon. En raison de cette nécessité de
simuler jusqu'à des temps assez longs (comparé à l'échelle de temps le plus
courte du système), l'algorithme \enquote{source-sink} est bien adapté, étant
donné la complexité linéaire de ce dernier.

Après une
introduction aux éléments pertinents de la théorie de la supraconductivité
(l'équation Bogoliubov-de Gennes et réflexion d'Andreev),
on étudie la phénomène de réflexion d'Andreev multiple (MAR) en raison duquel
un courant circule dans une jonction Josephson en dessous du \enquote{gap}
supraconducteur. Bien qu'on emploie une méthode résolu en temps pour étudier
un problème dont la solution est périodique, on atteint néanmoins un
\emph{accord quantitatif} entre nos résultats et un calcul analytique
basé sur la théorie de Floquet.

Après on passe à une situation où notre
point de vue \enquote{résolu en temps} à un net avantage: l'étude
d'impulsions de tension appliqué à une jonction Josephson
longue. On voit qu'on génère un \emph{courant périodique} même
avec un seul impulsion de tension, comme le montre la \cref{fig:sum_fr:jj}.
L'impulsion de tension génère une excitation dans la jonction qui
devient piégée, parce que l'impulsion de tension est suffisamment
courte pour que la tension retombe à zéro avant que l'excitation
n'ait le temps de traverser la jonction et revenir au contacte
de gauche (après une réflexion d'Andreev).

Finalement on traite des jonctions courtes, où le temps de vol à travers
la jonction est beaucoup plus court que la durée de l'impulsion de tension,
et on voit qu'on peut néanmoins obtenir un courant périodique après
une seule impulsion de tension. L'impulsion crée une excitation dans
une superposition du pair d'états liées d'Andreev dans la jonction,
qui sont à des énergies $E$ et $-E$, ce qui fournit un courant
qui oscille à une fréquence $2E/ℏ$.

\begin{figure*}[tb]
    \centering
    \includegraphics[width=0.6\textwidth]{josephson/DAR_current}
    \scaption{
        Courant (ligne bleue) et tension (ligne rouge à tireté, écarté
        verticalement) au contacte de gauche entre la région supraconductrice
        et la région normale en fonction du temps. \emph{Cartouche:} propagation
        de l'impulsion de charge à travers la jonction à des temps différents
        ($t_1$, $t_2$, $t_3$, $t_4$) et les temps correspondants indiqués dans
        la figure principale.
    }
    \label{fig:sum_fr:jj}
\end{figure*}


\subsection*{\color{ctcolorblack}Chapitre \ref{ch:maj}:
             \color{ctcolormain} Modification de Résonances d'Andreev et de Majorana dans des Nanofils}
Le chapitre final réunit les concepts d'interférométrie introduit dans le
chapitre~\ref{ch:fly} avec ceux de la supraconductivité et des états liés
d'Andreev du chapitre~\ref{ch:jj}.  On étudie un nanofil couplé à une
supraconducteur, un système qui a recueilli récemment beaucoup d'intérêt en
raison de l'état dîtes de \enquote{Majorana} qui peut s'y retrouver, dont la
signature est un pic dans la conductance différentielle à tension nulle.

On commence par traiter le système en absence d'état de Majorana, et montre
qu'en appliquant un \emph{train d'impulsions de tension} on peut manipuler
les pics dans la conductance différentielle en dessous du gap supraconducteur
qui proviennent des résonances d'Andreev. En particulier on peut décaler les
résonances à des tensions différentes an appliquant des trains de fréquences
différentes.

Après, on rajoute plus d'ingrédients à notre modèle (couplage de type Rashba
et couplage de type Zeeman), et on se place dans un régime de
\enquote{spin-momentum locking}, ce qui produit des états de Majorana
ainsi que le pic caractéristique de conductance différentielle à tension nulle.
On montre qu'avec la même technique avec un train d'impulsions, on peut
manipuler les résonances de Majorana de la même façon. On étudie l'effet
de ce train d'impulsions lorsqu'on modifie l'amplitude des impulsions
ainsi que la fréquence du train, et on est capable même de faire
de la \enquote{spectroscopie} de l'état de Majorana, comme la \cref{fig:sum_fr:maj}
le montre. Ceci révèle une signature distincte pour la mécanisme de réflexion
d'Andreev résonante qui donne lieu à l'état de Majorana.

On pourrait utiliser cette spectroscopie
comme \emph{sonde expérimentale supplémentaire} pour montrer
qu'un pic de conductance à tension nul vient réellement d'un
processus de réflexion d'Andreev résonante, ce qui fournirait
une preuve supplémentaire de sa caractère \enquote{Majorana}.

\begin{figure}[tb]
    \centering
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth]{majorana/omega_E_colorplot}
        \scaption{
            Conductance différentielle en présence d'une
            tension sinusoïdale avec $\bar{n}=0.5$ en fonction de la fréquence
            et la tension de biais.
        }
        \label{fig:maj:omega_E_colorplot}
    \end{subfigure}
    \hfill
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=0.8\textwidth]{majorana/omega_nbar_colorplot}
        \scaption{
            Conductance différentielle à tension nulle en présence d'une
            tension sinusoïdale en fonction de la fréquence et $\bar{n}$.
        }
        \label{fig:maj:omega_nbar_colorplot}
    \end{subfigure}
    \scaption{
        Conductance différentielle en présence d'un train d'impulsions
        de tension dans une jonction normale-isolant-normale-supraconducteur
        dans laquelle manifeste un état de Majorana.
    }
    \label{fig:sum_fr:maj}
\end{figure}

\end{otherlanguage}
% reset
\addtocontents{toc}{\protect\setcounter{tocdepth}{\tocdepth}}
\renewcommand{\thefigure}{\arabic{chapter}.\arabic{figure}}
