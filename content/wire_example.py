#! /usr/bin/env python3
# -*- coding: utf-8 -*-

# MARK imports start
import numpy  # useful numerical utilities
from matplotlib import pyplot  # plotting library
import kwant  # make Kwant available

lat = kwant.lattice.square()  # a square lattice for our sites
syst = kwant.Builder()  # an empty tight-binding system
# MARK imports end

# MARK system start
# populate the system with sites, and set the on-site
# Hamiltonian matrix elements to +4
for x in range(20):
    for y in range(10):
        syst[lat(x, y)] = 4

# set all the nearest-neighbour hoppings to -1
syst[lat.neighbors()] = -1

# define a function for the insulating barrier
def insulating_barrier(site, Vg):
    return 4 + Vg

# change onsite matrix elements for all sites under the barrier
for x in range(9, 12):
    for y in range(10):
        syst[lat(x, y)] = insulating_barrier
# MARK system end

# MARK lead start
lead = kwant.Builder(kwant.TranslationalSymmetry((-1, 0)))
for y in range(10):
    lead[lat(0, y)] = 4
lead[lat.neighbors()] = -1

# attach semi-infinite leads to the scattering region
syst.attach_lead(lead)  # lead from the left
syst.attach_lead(lead.reversed())  # lead from the right
# MARK lead end

# MARK finalise start
fsyst = syst.finalized()
# MARK finalise end

# MARK calculation start
Vg = 0.05
energies = numpy.arange(0, 1, 0.01)  # [0, 0.01, ..., 0.99]
transmissions = []
for E in energies:
    # calculate the scattering matrix at a given energy
    # with a given value of the system parameter(s) (args)
    smatrix = kwant.smatrix(fsyst, energy=E, args=(Vg,))
    T01 = smatrix.transmission(1, 0)  # lead 0 -> lead 1
    transmissions.append(T01)

pyplot.plot(energies, transmissions)  # plot the results
pyplot.xlabel(r'$E \ [\gamma]$')  # set x-axis label
pyplot.ylabel(r'$G \ [e^2/h]$')  # set y-axis label
pyplot.show()  # show the plot
# MARK calculation end
