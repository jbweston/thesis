% -*- coding: utf-8 -*-
%
\chapter{Software Design}
\label{ch:soft}
\def \imgpath {images/software/}
\graphicspath{{\imgpath}}

In \cref{ch:num} the main algorithms used to simulate time-resolved quantum
transport were presented. While the algorithms themselves are certainly
important, one could argue that of almost equal importance is a concrete software
implementation that actually delivers demonstrable added value to research
projects.
%
In this chapter we shall start by discussing the requirements and
\enquote{philosophy} for good scientific software in the context of exploratory
research.  We will then move on to identify the abstractions that allow one to
easily express the necessary concepts for time-resolved transport. In addition
we shall identify the state of our current implementation, \tkwant, and what
needs to be improved before it is ready for public release. Finally we will end
with a short gallery of examples of \tkwant usage from projects that were
mainly the work of collaborators, or where I personally had a role geared more
towards the software than the physics. This is to show that despite its warts,
the current implementation of \tkwant has been used to study a wide variety of
physics outside the main applications that will be presented in
\cref{part:applications} of this thesis, and has achieved the goal of
delivering demonstrable added value to research projects.


\section{Requirements for Well Designed Scientific Software}
\label{sec:soft:intro}
The latter half of the twentieth century saw massive advances in both
numerical algorithms and the computing power required to execute them.
Indeed, while numerical methods for solving mathematical problems
are not new (methods developed by Newton, Euler, and Gauss -- to
name but a few -- are at the core of many modern algorithms),
their great utility was only really unlocked at the advent of the
digital computer. What has also changed to some extent is the relationship
between numerical methods and their use in fundamental and applied research,
especially in physics.

One could say that the evolution of the physicist's use of numerical methods
parallels the evolution of the computer programmer's use of the hardware at her
disposal. Similarly to how the first general purpose, programmable computers
were programmed directly in machine language\footnote{Machine language is the
raw sequences of numbers that are interpreted and executed by the computer's
processor.}, the first uses of numerical methods by physicists were to solve
problems of very restricted scope: evaluating a specific integral, or solving a
particular differential equation. The appearance of \enquote{higher level}
programming languages freed the programmer from the tedious bookkeeping
required to write in machine language, and allowed her to more easily step back
and see the big picture.  Similarly, as more general purpose implementations of
algorithms were developed, the physicist was freed from worrying about the
implementation details and could instead concentrate on what matters: the
physics. This development has continued up until the present day, where very
high-level computing languages such as Python~\cite{python} and
Ruby~\cite{ruby} almost totally obviate the need to worry about the internals
of the computer.  Similarly, in physics, there is a proliferation of black-box
simulation software -- especially in the fields of computational chemistry and
nanoscience~\cite{qcm_list_2016} -- that obviates much of the need to
understand the fundamental algorithms used\footnote{Recent technological
advances even obviate the need to write a scientific
article~\cite{labbe_duplicate_2012} and get it published in a peer review
journal~\cite{van_noorden_publishers_2014}, but I digress.}.
%
While the parallel advances in programming languages and physics software have
brought many positives, there are also negative consequences.  In particular,
high-level programming languages tend to produce programs that run more
slowly\footnote{A function call in Python is roughly 100 times slower than a
function call in the C language.}, and black-box physics software hides perhaps
too much from the researcher using it.

In my opinion there is a missing abstraction layer for quantum transport in
the current offering of software tools. At the lowest layer of abstraction
there are a very solid set of interfaces for solving specific mathematical
problems, such as \textsc{blas} and \textsc{lapack} for dense linear
algebra~\cite{blackford_updated_2002, anderson_lapack_1999}, or
\textsc{quadpack} for numerical integration~\cite{piessens_quadpack_1983}. At
the other end of the spectrum there are the professionally developed (and often
non-free\footnote{I use the word \enquote{free} here, as defined by the Free
Software Foundation~\cite{free_software}, to refer to software that respects
users' freedoms, rather than software that is \emph{gratis}.})
tools~\cite{nextnano,atomistix,nemo} that provide an enormous amount of
functionality, but are often too rigid for exploratory research.  In the middle
there are a whole host of \enquote{home made} tools that are often used by only
a single research group and passed down from student to student, each
generation making their contribution: adding support for spin, allowing for a
ring geometry with a hole, etc. The consequence of this is that there is an
enormous amount of replicated effort. There is a real need to create an
abstraction layer for the fundamental objects of quantum transport:
Hamiltonians, Green's functions, scattering matrices, etc., that empowers
researchers to easily set up and solve well-defined physical problems without
having to be concerned about the low-level details. The researcher nevertheless
needs to have the freedom to be able to set up \emph{exactly} the calculation
that they want, so such an abstraction layer should not \enquote{hard code}
assumptions about the physics being studied.

\subsection{\kwant: An Example of Well Designed Scientific Software}
The \kwant package aims to be this missing layer in the software stack. \kwant
essentially provides an interface for specifying a tight-binding Hamiltonian
and computing quantities relevant for d.c.\ transport such as band structures,
scattering matrices, and Green's functions. Notably, \kwant leaves the
definition of the tight-binding Hamiltonian to the user; whether such a
description was arrived at by discretising a continuum model or extracting
tight binding parameters from a density functional theory calculation, is
largely immaterial from \kwant's point of view.

Another notable difference between \kwant and traditional scientific codes is
that \kwant is actually a \emph{library} for the Python programming language,
which means that simulations that use \kwant are written as small Python
programs. This approach has several distinct advantages over the more
traditional approach to scientific software, where a monolithic binary
executable takes an input file, performs a computation, and produces an output
file. Firstly, the Python programming language is simple to learn and offers
almost infinite extensibility.  Compare this to an input file approach where as
soon as you want to express anything reasonably complex you have to essentially
invent your own domain specific language in order to do it; re-implementing
elementary concepts like variables, loops, and sequences. Secondly, there is an
enormous ecosystem of extensions to the Python language~\cite{cheeseshop}
(called \enquote{packages}), that allow one to efficiently manipulate numeric
arrays~\cite{van_der_walt_numpy_2011}, do symbolic algebra
operations~\cite{sympy}, or produce publication-quality
graphics~\cite{hunter_matplotlib:_2007}. This means that \kwant users (as well
as \kwant developers) can leverage all this power in their scripts and simulations.  Finally,
while Python is a somewhat slow language it is also straightforward to write
extension modules in low-level and fast languages like C and Fortran
~\cite{behnel_cython:_2011}, which means that \kwant does not have to sacrifice
run-time speed in the name of ease of use.

\subsubsection{A Simple Example of Kwant Usage}
To give more of a feel for how \kwant is actually used we will now go through a
simple example of a quantum wire with a variable-height insulating barrier. We
shall identify the three major steps in a \kwant simulation: system definition;
finalisation of the system into a low-level format for efficient computation;
and solution of the scattering problem. This will allow us to more easily
appreciate the changes needed to incorporate time-resolved transport.  This
example should be comprehensible to readers who do not know the Python
language, as long as they are familiar with programming concepts such as loops
and subroutines (functions).  Ref.~\cite{groth_kwant:_2014} provides a more
complex example for the interested reader.

The first thing we need to do is to make \kwant available and define our
lattice and an empty tight-binding system (any text on a line after a
\texttt{\#} is a comment):
%
\lstinputlisting[linerange={imports}]{content/wire_example.py}
%

Next, we need to define our tight-binding model, which we obtain from
discretising a continuum Hamiltonian onto a square lattice following
\cref{sec:app:discretising}. First we define the scattering region, which is 20
sites long and 10 sites wide and contains the potential barrier (3 sites wide)
that will induce backscattering:
%
\lstinputlisting[linerange={system}]{content/wire_example.py}
%
We see that we can set Hamiltonian matrix elements in two ways: with explicit
values, and with \emph{functions}. The functions will be evaluated at the last
possible moment, when the Hamiltonian is requested; this allows for a system to
be constructed once at the start of the script, and then solved many times for
different values of the system parameters. In the above example there is a
single parameter \texttt{Vg}, which controls the onsite matrix element for a
strip three sites wide in the centre of the scattering region.

Next we define the electrodes as tight-binding systems with a translational
symmetry in the $x$ direction, and we attach them to the scattering region:
%
\lstinputlisting[linerange={lead}]{content/wire_example.py}
%

Next we convert the system into a low-level format suitable for numerics:
%
\lstinputlisting[linerange={finalise}]{content/wire_example.py}
%
The consequence of this is that the geometry (or in other words the sparsity
structure of the Hamiltonian) is fixed.

Finally we are ready to compute the scattering matrix and transmission
for the system:
%
\lstinputlisting[linerange={calculation}]{content/wire_example.py}
%
This script produces the plot of differential conductance as a function of
energy shown in \cref{fig:soft:example}.  So we see that in 46 lines of simple
code (the line count could be drastically reduced with more terse code) we can
reproduce an elementary result of quantum transport: conductance
quantisation.
\begin{figure}[tb]
    \centering
    \includegraphics[width=0.6\textwidth]{example_plot}
    \scaption{Differential conductance through a quasi one-dimensional wire
    with an insulating barrier. This exact figure was produced from the script
    shown in the main text; an illustration of the simplicity of the \kwant
    package.
    }
    \label{fig:soft:example}
\end{figure}


\section{\tkwant: Time-Dependent Extensions to \kwant}
\label{sec:soft:arch}
In its current iteration \kwant has no facilities for simulating
time-dependent quantum transport.
%
Part of the work of the past three years has been not only to develop the
algorithms necessary for an efficient implementation of time-dependent quantum
transport, but to identify the correct abstractions that will allow
time-dependent transport to be added seamlessly to \kwant.  In the following
paragraphs we will discuss how the three stages of \kwant usage (system
definition, finalisation, solving) would have to be altered (or what would have
to be added) in order for \kwant to support time-dependent problems.

\subsection{Modifications to the Problem Definition Stage}
The addition when specifying a time-dependent system is that some elements of
the Hamiltonian should depend on time.  As we saw in the above example \kwant
already supports assigning functions to Hamiltonian elements, so naturally any
parts of the Hamiltonian that depend on time just need to take a \texttt{time}
parameter. This is the way that specifying time-dependence is implemented in
the current version of \tkwant.

A second aspect, which is not present in \kwant, is how to specify the
observables that should be calculated during the solving phase. To be generally
useful one should be able to supply a function that receives the \kwant system,
the current time, and the scattering state wavefunction as inputs, and returns
the action of the observable on the wavefunction. This is the interface used
by the current version of \tkwant.
%
Although such an interface is sufficiently general to capture all use cases, it
is actually too general to be useful for most users.  One particularly useful
specialisation would be the case of general \emph{densities} and their
associated \emph{currents}. We can define a density associated with a site $i$
as
\begin{equation}
    ρ^M_i(t) = \vb{ψ}_i^\dagger(t)\,\vb{M}_i(t) \vb{ψ}_i(t)
\end{equation}
where $\vb{ψ}_i(t)$ is a vector of wavefunction components on site $i$ (e.g.
spin or particle/hole degrees of freedom), and $\vb{M}_i(t)$ is a (possibly
time-dependent) Hermitian matrix associated with site $i$.  The associated
current between sites $i$ and $j$ can be written
\begin{equation}
    J^M_{ij} = 2\Im\qty{\vb{ψ}_i^\dagger(t)\,\vb{M}_i(t)\vb{H}_{ij}(t)\vb{ψ}_j(t)]}.
\end{equation}
In the case of spin, for example, the $\vb{M}_i$ could be Pauli matrices, and
the $ρ^M_j$ would therefore be a \enquote{spin density}. In order for a user to
specify their density observable it would therefore be sufficient to provide a
function that takes a site and a time and returns the appropriate Hermitian
matrix representing the density for that site. This would be similar to the way
in which the system Hamiltonian is defined with functions that take sites, as
well as other parameters, and returns the Hamiltonian matrix elements. Building
observables in this way could also be useful for \kwant users irrespective of
\tkwant.


\subsection{Modifications to the Finalisation Stage}
The \kwant low-level system format allows for the Hamiltonian to be evaluated,
and for the structure of the system to be efficiently queried (e.g. which sites
are connected via hoppings etc.). \tkwant requires an additional component:
evaluation of the time-dependent part of the Hamiltonian. Often the time
dependence of a system will only affect a limited number of matrix elements,
and in such cases it is wasteful to re-evaluate the full Hamiltonian. The
low-level format required by \tkwant therefore needs an efficient way to query
the system as to which matrix elements depend on time, and only re-evaluate
these. In the current version of \tkwant a new type of low-level system
is used for this purpose, and \kwant systems are finalised into this new
\enquote{\tkwant finalised system}.


\subsection{Modifications to the Solving Stage}
The solving stage will clearly be vastly different for \tkwant when compared
with \kwant. While the \kwant solvers all boil down essentially to solving a
linear system (scattering matrix and scattering wavefunction calculations) or
an eigenvalue problem (calculating band structure), the source-sink
algorithm at the heart of \tkwant requires evolving many differential equations
(one for each mode and value of momentum/energy) and numerically integrating
the results. The \tkwant solver will therefore be completely independent
from the other \kwant solvers, but will need to use them in order to obtain
the initial conditions for the scattering states. A user of the solver
should simply be able to provide: a system that depends on time,
a list of observables to calculate, and the times at which to evaluate
them, and get back the thermal average of the observables evaluated
at those times.
%
One challenge to overcome is that there are many more parameters associated
with the source-sink algorithm: what boundary conditions to use, what algorithm
to use to evolve the scattering states, and what algorithm to use for the
integrals over momentum/energy.  There is also the question of how errors will
accumulate during the calculation.
%
The choices made for the current implementation
were discussed in \cref{sec:num:robust,sec:num:timestepping,sec:num:integral}.
A future version of \tkwant should certainly allow for arbitrary combinations
of boundary conditions/steppers/integrators.

Another challenge is that an efficient implementation of the source-sink
algorithm will necessarily involve parallel computations, as each
$\psi_{αk}(t)$ evolves independently of the others.  The current version of
\tkwant exploits this available parallelism, however it does not implement an
adaptative energy/momentum integration (see the last paragraph of
\cref{sec:num:integral}).  Instead, one merely chooses a set of integration
regions at the start of the calculation and the observables (and an error
estimate) are calculated using these regions during the whole calculation. This
means that the convergence (or not) of the calculation can only be verified at
the end.
%
While this mode of operation is useful when a given set of integration regions
is known to produce results that converge, it is particularly inefficient when
no knowledge of the structure of the observables in energy/momentum is known.
In this (common) case one has to manually inspect the integrand and choose the
integration regions accordingly. A future version of \tkwant should certainly
have an adaptative energy/momentum integration.  This somewhat complicates the
parallel implementation, however, as whenever a region is subdivided the
wavefunctions need to be recomputed for the new energies/momenta. The question
is then how to best divide the available computing resources between the
various sub-regions.



\section{\tkwant Usage in the Field: A Gallery of Examples}
\label{sec:soft:examples}
In this section we will see a small gallery of examples from work done
primarily by my collaborators, which will complement the applications from my
own work that will be seen in \cref{part:applications}.
%
While the interface of the current version of \tkwant is somewhat rough around
the edges, the results presented in this section show nevertheless that it is a
general tool that is capable of bringing real added value.


\subsection{Calculating Time-Resolved Shot Noise}
\label{sec:soft:shotnoise}
Single particle observables such as the charge and current can generally
be written in terms of a single-particle Green's function. In the
wavefunction formalism, these can be written as a single integral over
energy, as seen in \cref{sec:num:define}. Often, one wants to go beyond
such simple observables and instead look at higher-order cumulants.
Current-current correlations, for example, can be written as
\begin{equation}
    \qop{S}_{μν}(t, t') =
        \qty(\qop{I}_μ(t) - \expval{\qop{I}_{μ}(t)}) \times
        \qty(\qop{I}_ν(t') - \expval{\qop{I}_{ν}(t')})
\end{equation}
where $\qop{I}_μ(t)$ is the current operator for current flowing
across an interface $μ$:
\begin{equation}
    \qop{I}_μ(t) = ∑_{(i,j) \in μ} \ham_{ij}(t)\cqop{c}_i(t)\qop{c}_j(t) - \ham_{ji}(t)\cqop{c}_j(t)\qop{c}_i(t),
\end{equation}
and $\expval{\qop{I}_μ(t)}$ is its thermal average. We can see that
$\qop{S}_{μν}(t, t')$ will contain products of four operators and hence, using
Wick's theorem, its thermal average can be expressed as products of \emph{two}
Green's functions.

In ref.~\cite{gaury_computational_2016} expressions are derived for the
current-current correlations in terms of the time-evolved scattering
wavefunctions. Specifically, the thermal average of the current-current
correlations can be written as
\begin{equation}
    \expval{\qop{S}_{μν}(t, t')} = ∑_{αβ} ∫\frac{\dd{E}}{2π}∫\frac{\dd{E}'}{2π}
        f_α(E)[1 - f_β(E')] I_{μ,EE'}(t)[I_{ν,EE'}(t')]^*
\end{equation}
where
\begin{equation}
    I_{μ,EE'}(t) = ∑_{(i,j) \in μ} \qty(
        \ham_{ij}(t)[ψ^\dagger_{βE'}(t)]_i [ψ_{αE}(t)]_j - \ham_{ji}(t)[ψ^\dagger_{βE'}(t)]_j[ψ_{αE}(t)]_i
    ).
\end{equation}

In ref.~\cite{gaury_computational_2016} they did not numerically compute such
quantities directly, however, opting instead to look at the variance of the
particle number operator:
\begin{equation}
    \qop{n}_μ = ∫_{-τ/2}^{τ/2} \dd{t} \qop{I}_μ(t),
\end{equation}
which they split into three contributions in order to take care of terms
divergent in the limit $τ\to\infty$, which come from the contribution
from the equilibrium. Specifically,
\begin{equation}
    \label{eq:soft:noise}
    \variance\qty(\qop{n}_μ) \equiv \expval{\qop{n}^2_μ - \expval{\qop{n}_μ}^2} =
    τσ_{\mr{st}}^2 + 2σ_{\mr{mix}} + \bar{σ}^2 + \order{\frac{1}{τ}}
\end{equation}
where the three contributions are defined as
\begin{align}
    \label{eq:soft:noise-contribs}
    σ^2_{\mr{st}} &= ∑_{αβ} ∫ \frac{\dd{E}}{2π} f_α(E)[1 - f_β(E)] \abs{I_{μ,EE}(0)}^2\\
    σ_{\mr{mix}} &= ∑_{αβ} ∫ \frac{\dd{E}}{2π} f_α(E)[1 - f_β(E)] \Re{\bar{N}^*_{EE} I_{μ,EE}(0)}\\
    \bar{σ}^2 &= ∑_{αβ} ∫ \frac{\dd{E}}{2π}∫\frac{\dd{E'}}{2π} f_α(E)[1 - f_β(E')] \abs{\bar{N}_{EE'}}^2
\end{align}
where
\begin{equation}
    \bar{N}_{EE'} = ∫_{-\infty}^{\infty} \dd{t} \qty[I_{μ,EE'}(t) - I_{μ,EE'}(0)e^{-i(E-E')t/ℏ}].
\end{equation}

\begin{figure}[tb]
    \centering
    \subcaptionbox{\captionstyle Sketch of the one-dimensional wire system with
        insulating barrier used for calculating the noise in the number of
        transmitted particles. A gaussian voltage pulse $V(t)$ is applied to the
        left-hand contact, which injects charge into the system. The insulating
        barrier, controlled with $V_g$, provides backscattering. The number
        of particles traversing the cross-section $μ$ is measured, as is its
        noise properties.
        \label{fig:soft:noise-setup}
    }[0.49\textwidth]{\includegraphics[width=0.49\textwidth]{noise_wire}}
    ~
    \subcaptionbox{\captionstyle
        The variance in the number of particles $n_μ$ traversing the cross-section
        $μ$ versus the number of injected particles $\bar{n}$, computed from
        \tkwant simulations using \cref{eq:soft:noise,eq:soft:noise-contribs}.
        \label{fig:soft:noise-results}
    }[0.49\textwidth]{\includegraphics[width=0.49\textwidth]{noise_noise}}
    %
    \scaption{ Computing the noise in a quantum wire after the application of a
        voltage pulse.  Both subfigures reproduced with permission from
        ref.~\cite{gaury_computational_2016}.
    }
    \label{fig:soft:noise}
\end{figure}
They then use the raw output of \tkwant (the time-evolved scattering states) to
calculate $\variance\qty(\qop{n}_μ)$ for a one-dimensional conductor with an
insulating barrier (shown in \cref{fig:soft:noise-setup}), when a voltage pulse
is applied to one of the contacts. In addition they derived an analytical
result for $\variance\qty(\qop{n}_μ)$ in this specific case and compared it to
simulation; the results are shown in \cref{fig:soft:noise-results}, when the
area underneath the voltage pulse $\bar{n}$ ($= (e/h)∫ V(t) \dd{t}$)  is
varied. They saw that $\variance\qty(\qop{n}_μ)$ is \emph{minimised} for
integer values of $\bar{n}$, in analogy to the Levitons discussed in the
introduction~\cite{dubois_integer_2013}.

Although this result is in itself intriguing, it also paves the way for other
time-resolved noise properties to be calculated with \tkwant. We note, however,
that the problem required some delicate refactoring in order to avoid problems
with infinities related to the equilibrium contributions, which may hinder
attempts to make a robust implementation in the general case. The fact that
there are now \emph{two} energy integrals to calculate also somewhat
complicates matters. However, the fact that the two-energy quantity
$I_{μ,EE'}(t)$ can be expressed in terms of just the single-energy quantity
$ψ_{αE}(t)$ indicates that the computational effort will still scale linearly
as a function of the number of energy points.  This is because computing
$ψ_{αE}(t)$ on a set of energies $λ=\{E_0,E_1,\ldots,E_N\}$ allows us to
directly evaluate $I_{μ,EE'}(t)$ on the \emph{grid} $λ \cross λ =
\{(E_0,E_0), (E_0,E_1),\ldots, (E_{N},E_{N-1}), (E_N,E_N)\}$. Whether the
density of points required to reach convergence will be higher when calculating
the noise compared to the current is not clear.


\subsection{Stopping Voltage Pulses in the Quantum Hall Regime}
\label{sec:soft:qhe}
The integer quantum Hall effect (IQHE) is observed in two-dimensional
systems subject to strong magnetic fields~\cite{klitzing_new_1980},
characterised by an insulating bulk and unidirectional edge
states~\cite{buttiker_absence_1988}.  While most applications of the IQHE
concentrate on the properties of these edge states, in
ref.~\cite{gaury_stopping_2014} \tkwant is used to investigate the crossover
between these edge states and the bulk insulating states.
\Cref{fig:soft:qhe_pulse} shows a sketch of the simulated setup: a
two-dimensional system in the quantum Hall regime (light orange) is connected
to two electrodes (dark brown) and a gate with potential $V_g(t)$ is
electrostatically coupled to the system (dashed red box).  A voltage pulse is
applied to the lower contact, which injects charge into the system (the dark
blob moving through the system in \cref{fig:soft:qhe_pulse}).  Initially the
gate is at a potential $V_0$, which means that the right-hand \enquote{edge} of
the system is found in the centre, and we see that the charge pulse propagates
along this edge channel. Afterwards, at a time $t_1$, the gate voltage is
lowered until time $t_2$ when it becomes $0$. This effectively shifts the
right-hand edge channels to the true edge of the system. We can see from
\cref{fig:soft:qhe_pulse}, however, that the charge pulse does \emph{not}
follow the evolution of the edge channel; instead, it stays in the centre of
the system and its velocity decreases. We see that after $t_2$ the pulse has
completely stopped in the centre of the system. This behaviour is independent
of the time $t_2 - t_1$ over which the gate voltage is lowered, provided that
this time is not so long as to let the charge pulse escape through the upper
contact. The fundamental reason for this highly non-intuitive result is that
the system remains translationally invariant along the $y$ direction at all
times, which means that the quasimomentum $k$ must be conserved (a more
in-depth explanation is given in refs.~\cite{gaury_stopping_2014,
gaury_emerging_2014}).
\begin{figure}[tb]
    \centering
    \includegraphics[width=0.8\textwidth]{qhe_pulse_snapshots}
    \scaption{
        \emph{Upper}:
        Snapshots of the deviation from equilibrium of the charge density as a
        function of time in a two-dimensional system in
        the quantum Hall regime. The dark \enquote{blob} is the charge
        pulse discussed in the main text. The system (beige) is connected
        to two electrodes (dark brown) and a gate voltage is applied on top
        of the right-hand portion of the system (red dashed box). The
        gate voltage is initially $V_0$, and is reduced to $0$ between
        $t_1$ and $t_2$.
        \emph{Lower}:
        Velocity of the charge pulse as a function of time. \tkwant
        is used to calculate $\expval{\qop{y}(t)}$ and this is differentiated
        numerically.
        Reproduced with permission from ref.~\cite{gaury_stopping_2014}.
    }
    \label{fig:soft:qhe_pulse}
\end{figure}

Intuitively one would perhaps think that the charge pulse would follow the edge
channel as the gate voltage is decreased, however the simulations indicate that
this intuition is incorrect.  This is a prime example of where numerics in
general (and \tkwant specifically) offer a great deal of added value. Without
the initial insight offered by the \tkwant simulations, it would initially not
have been clear that there was any interesting physics to be seen in the
above-described setup. While a deeper understanding will usually be found by
employing analytical methods, the easy-to-perform numerics afforded by \tkwant
offer increased agility and ability to concentrate on the \enquote{big
picture}, which is invaluable in the initial stages of an exploratory
research project.


\subsection{a.c. Josephson Effect Without Superconductivity}
\label{sec:soft:ac-josephson}
In many of the applications in \cref{part:applications} we will see that
quantum interference will play a big role. In ref.~\cite{gaury_.c._2015} it was
shown that after an abrupt change of bias voltage  across a quantum
interferometer there is a universal regime where the current measured at the
output of the interferometer oscillates at a frequency $eV_b/ℏ$ ($V_b$ the bias
voltage) during a certain time $τ_F$. While the duration of this universal
regime depends on the specific system studied, the frequency does not. This was
interpreted as the analogue of the a.c.\ Josephson effect for normal
conductors. The fundamental idea is that the application of a bias voltage
changes the frequency at which the electronic wavefunctions oscillate, from
$E/ℏ$ to $(E+eV_b)/ℏ$. This frequency change originates at the voltage drop (it
is assumed that this drop is spatially short, and occurs before the actual
interferometer) and propagates through the system at the electronic group
velocity.  As the interferometer will naturally have multiple paths of
different lengths, the frequency change will take different times to propagate
along each path. Once the frequency change has arrived from the shortest path,
the part of the wavefunction oscillating at $(E+eV_b)/ℏ$ will interfere with
the part oscillating at $E/ℏ$, giving rise to oscillations of frequency
$eV_b/ℏ$. In ref.~\cite{gaury_.c._2015} a Mach-Zehnder interferometer in the
quantum Hall regime is studied due to its simplicity (only two paths are
avalable through the system due to the quantised edge channels); this is
illustrated in \cref{fig:soft:ac_mz}.  \Cref{fig:soft:ac_results} shows the
current measured at contact $1$ as a function of time after the bias on contact
$0$ is raised to $V_b$ at $t=0$. We clearly see that the output current
oscillates at frequency $eV_b/ℏ$ for a finite time before we reach the steady
state regime that one would expect from a d.c.\ bias.

The strength of \tkwant in this work was that it was easy to set up simulations
for other types of interferometers (a Fabry-Perot was also studied in
ref.~\cite{gaury_.c._2015}), which enabled the claim of a \enquote{universal}
transient regime to be corroborated.
\begin{figure}[tb]
    \centering
    \subcaptionbox{\captionstyle
        Snapshots of the charge density (measured from equilibrium) in the
        Mach-Zehnder interferometer at different times after the bias on
        contact $0$ is raised, from vanishing density (yellow) to
        $10^{11}\si{\per\square\cm}$ (red).
        \label{fig:soft:ac_mz}
    }[0.49\textwidth]{\includegraphics[width=0.49\textwidth]{ac_mach_zehnder}}
    ~
    \subcaptionbox{\captionstyle
        Current at contact $1$ as a function of time. \emph{Upper inset}: Schematic
        of the voltage rise. \emph{Lower inset}: zoom of the transient regime.
        The coloured crosses on the time axis correspond to the times at which
        the snapshots in \cref{fig:soft:ac_mz} are taken.
        %
        \label{fig:soft:ac_results}
    }[0.49\textwidth]{\includegraphics[width=0.49\textwidth]{ac_results}}
    %
    \scaption{Results from \tkwant simulations of the Mach-Zehnder interferometer
    after an abrupt raise of bias voltage on contact $0$.
    %
    Subfigures reproduced (modified) with permission from ref.~\cite{gaury_.c._2015}.
    }
    \label{fig:soft:ac}
\end{figure}

\subsection{Floquet Topological Insulators}
\label{sec:soft:topological}
Systems with so-called \enquote{topological} phases have recently seen a great
deal of theoretical as well as experimental interest~\cite{hasan_colloquium_2010}.
Such systems are characterised by a bulk that is
insulating and sufaces that support chiral edge states, and are thus are
conducting.  One simple example of this is the
quantum Hall regime that can be induced in two-dimensional electron gases by
applying a strong perpendicular magnetic field\cite{klitzing_new_1980,
thouless_quantized_1982}. A more recent development involves inducing
topological phases by applying \emph{periodic perturbations} to the system;
these are known as Floquet topological
insulators~\cite{kitagawa_topological_2010, dahlhaus_quantum_2011,
cayssol_floquet_2013}.

In ref.~\cite{fruchart_probing_2016} \tkwant is used to show that there is a
correspondence between the differential conductance and the quasienergy
spectrum of a Floquet topological insulator that arises from the
Bernevig-Hughes-Zhang (BHZ) model~\cite{rudner_anomalous_2013,
bernevig_quantum_2006}, which is used as a model for mercury telluride/cadmium
telluride quantum wells~\cite{bernevig_quantum_2006, konig_quantum_2007}.
Schematically, the half-BHZ model corresponds to a square lattice model with
two orbitals per site and first and second nearest neighbour hoppings:
\begin{equation}
\begin{split}
    \qop{\ham} = ∑_{x,y}\Big[&
        \big(Aσ_3 - Bσ_0\big)\ketbra{x,y}{x,y} +
        Cσ_0\ketbra{x,y}{x+1,y} + Dσ_0\ketbra{x,y}{x,y+1} +\\
        &Jσ_0\big(\ketbra{x,y}{x+1,y+1} + \ketbra{x,y}{x+1,y-1}\big)
    \Big],
\end{split}
\end{equation}
where capital latin letters denote constant scalars that parametrise the model,
and $σ_j$ are Pauli matrices:
\begin{equation}
    σ_0 = \mqty(1&0\\0&1) \;,\quad σ_1 = \mqty(0&1\\1&0) \;,\quad
    σ_2 = \mqty(0&-i\\i&0) \;,\quad σ_3 = \mqty(1&0\\0&-1).
\end{equation}
Additionally, a periodic on-site perturbation
\begin{equation}
    Δ\ham_{\mr{BHZ}} = ∑_{x,y} F[\sin(ωt)σ_1 + \cos(ωt)σ_2]
\end{equation}
is added that can induce a topological phase.  As the Hamiltonian is
time dependent energy is no longer conserved, however -- as the
perturbation is periodic with period $T$ -- the \emph{quasienergy} is still a
useful quantity.  The quasienergies $ε_α$ are defined by the eigenvalues of the
\emph{Floquet operator} $\qop{U}(T, 0)$:
\begin{equation}
    \qop{U}(T, 0)\ket{φ_α} = e^{-i(ε_α - iη_α)T/ℏ}\ket{φ_α},
\end{equation}
where $η_α/ℏ$ is a damping rate, and $\ket{φ_α(t)}$ is a right eigenstate of
the Floquet operator. The Floquet operator itself is just the evolution
operator evaluated over one period:
\begin{equation}
    \qop{U}(T, 0) = \mathcal{T}\qty[e^{-(i/ℏ)∫_0^T \qop{\ham}(t) \dd{t}}],
\end{equation}
where $\mathcal{T}$ is the time-ordering operator.

First the case of a two-terminal quasi one-dimensional system is analysed. The
model is placed in a parameter regime that should exhibit topological states
and the quasienergy spectrum is calculated analytically, while \tkwant is used
to calculate the time average of the differential conductance.
\Cref{fig:soft:topo_spectrum} shows this quasienergy spectrum and differential
conductance. The differential conductance clearly shows two peaks where the two
bands of quasienergies occur, and the conductance drops to a \emph{finite}
value $e^2/h$ in the \enquote{gap} between the bands.  This finite conductance
is due to the presence of topological edge states inside the gap (shown
as the red/blue symbols in the quasienergy spectrum).

\begin{figure}[tb]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \includegraphics[width=0.5\textwidth]{topo_spectrum}
        \scaption{Quasienergy spectrum (left) and the differential conductance
        (right) for the two-terminal setup when the model is tuned to exhibit a
        Floquet topological phase.  The green symbols in the spectrum denote
        Floquet states in the quasienergy bands, and the red/blue symbols
        denote the topological Floquet states in the gap. The differential
        conductance shows finite quantisation $e^2/h$ in the quasienergy gap.
        }
        \label{fig:soft:topo_spectrum}
    \end{subfigure}
    \par\bigskip
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{topo_nontopo}
        \scaption{Conductance as a function of time for the
        three-terminal setup, at the Fermi energy indicated
        by the green arrow in \cref{fig:soft:topo_spectrum}.
        \emph{Inset}: probability density map at $t/T=1800$,
        showing the delocalised Floquet state.
        }
        \label{fig:soft:topo_nontopo}
    \end{subfigure}
    ~
    \begin{subfigure}{0.49\textwidth}
        \centering
        \includegraphics[width=\textwidth]{topo_topo}
        \scaption{Conductance as a function of time for the
        three-terminal setup, at the Fermi energy indicated
        by the red arrow in \cref{fig:soft:topo_spectrum}.
        \emph{Inset}: probability density map at $t/T=1800$,
        showing the Floquet state localised on one edge.
        }
        \label{fig:soft:topo_topo}
    \end{subfigure}
    \scaption{Results from \tkwant simulations of the half-BHZ model (see main
    text). Subfigures reproduced with permission from
    ref.~\cite{fruchart_probing_2016}.
    }
\end{figure}
This can be seen even more clearly by performing a three-terminal measurement
in a T-junction geometry. \Cref{fig:soft:topo_nontopo,fig:soft:topo_topo} show
the three-terminal differential conductance as a function of time after the
perturbation is switched on at two different chemical potentials.
\Cref{fig:soft:topo_nontopo} corresponds to a chemical potential inside the
band of quasienergies, whereas \cref{fig:soft:topo_topo} corresponds to a
chemical potential in the gap (these are marked by arrows on the y-axis in
\cref{fig:soft:topo_spectrum}). In the former case we clearly see that the both
the conductances take unquantised, finite values and the inset of
\cref{fig:soft:topo_nontopo} shows the delocalised nature of the Floquet state.
In the latter case, however, we clearly see that the conductance between
the left and top contacts remains zero while the between the right and top
contacts the conductance is finite and (nearly) quantised to $e^2/h$.
In addition the inset of \cref{fig:soft:topo_topo} clearly shows the localised
nature of the Floquet state inside the gap.

The use of \tkwant in this work clearly demonstrates its applicability to
purely periodic problems, despite the fact that it is a time-resolved approach.
