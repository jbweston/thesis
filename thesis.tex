% -*- coding: utf-8 -*-
%
\documentclass[
    paper=a4,                    % paper size
    twoside=true,                % onesite or twoside printing
    openright,                    % doublepage cleaning ends up right side
    chapterprefix=true,            % prefix for chapter marks
    12pt,                        % font size
    headings=normal,            % size of headings
    bibliography=totoc,            % include bib in toc
    listof=totoc,                % include listof entries in toc
    titlepage=on,                % own page for each title page
    numbers=noenddot            % no dot at end of numbers
]{scrreprt}

%% define author and title for cover and PDF metadata
\newcommand{\thesisTitle}{Numerical methods for time-resolved quantum nanoelectronics}
\newcommand{\thesisAuthor}{Joseph WESTON}
\newcommand{\tocdepth}{2}

\newcommand{\scaption}[1]{\caption{\footnotesize #1}}
\newcommand{\captionstyle}{\footnotesize}


% ---------------------
% Package Includes
% ---------------------

%% mathematics
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{physics}  % bra-ket notation, derivatives etc.
\usepackage{siunitx}

%% fonts
\usepackage{mathspec}
\usepackage{fontspec}
\usepackage{unicode-math}
\setmainfont{TeX Gyre Pagella}
\setsansfont{Linux Biolinum O}
\setmathfont{TeX Gyre Pagella Math}
\setmathfont[range=\symcal]{TeX Gyre Bonum Math}
\setmonofont{FreeMono}

%% language and graphics
\usepackage[main=english,french]{babel}
\usepackage[autostyle=true,french=guillemets]{csquotes}
\usepackage{graphicx}
\usepackage{color}
\definecolor{grey}{rgb}{0.95,0.95,0.95}
\usepackage{subcaption}
\captionsetup{subrefformat=parens}



%% misc
\usepackage{hyperref}
\hypersetup{                    % setup the hyperref-package options
    pdftitle={\thesisTitle},    %     - title (PDF meta)
    pdfauthor={\thesisAuthor},    %     - author (PDF meta)
    plainpages=false,            %     -
    colorlinks=false,            %     - colorize links?
    pdfborder={0 0 0},            %     -
    breaklinks=true,            %     - allow line break inside links
    bookmarksnumbered=true,        %
    bookmarksopen=true            %
}
\usepackage{microtype}
\usepackage{cleveref}
\usepackage{listings}
\lstset{
    language=Python,
    basicstyle=\footnotesize\ttfamily,
    backgroundcolor=\color{grey},
    keywordstyle=\bfseries,
    commentstyle=\itshape,
    xleftmargin=2\parindent,
    xrightmargin=2\parindent,
    rangeprefix={\#\ MARK\ },
    rangebeginsuffix={\ start},
    rangeendsuffix={\ end},
    includerangemarker=false,
}


%% cover
\usepackage{styles/cover}

%% bibliography
\RequirePackage[
    backend=bibtex,
    bibencoding=utf8
    style=numeric-comp,
    citestyle=numeric-comp,
    sorting=none,
    natbib=true,
    hyperref=true,
    backref=false,
    isbn=false,
    url=false,
    doi=false,
    urldate=long,
    clearlang=false,
]{biblatex}
\AtEveryBibitem{
    \clearlist{language}
}

% workaround for regression in biblatex
% (see http://tex.stackexchange.com/questions/311426/bibliography-error-use-of-blxbblverbaddi-doesnt-match-its-definition-ve)
\makeatletter
\def\blx@maxline{77}
\makeatother

\addbibresource{references.bib}
\addbibresource{special_references.bib}

%% cleanthesis main config
\usepackage[
    figuresep=colon,
    hangfigurecaption=false,
    hangsection=false,
    hangsubsection=false,
    colorize=full,
    colortheme=bluemagenta,
]{styles/cleanthesis}

%% set up margins and binding correction
\typearea[5mm]{13}


% --------------------------
% Maths defines
% --------------------------
%% quantum stuff
\newcommand{\qop}[1]{\ensuremath \hat{#1}}  % quantum operator
\newcommand{\ucqop}[1]{\ensuremath \hat{\mathrm{#1}}^\dagger}  % quantum operator
\newcommand{\uqop}[1]{\ensuremath \hat{\mathrm{#1}}}  % quantum operator
\newcommand{\cqop}[1]{\ensuremath \hat{#1}^\dagger}  % quantum operator
\newcommand{\ham}{\ensuremath \mathrm{H}}  % upright
\newcommand{\rGreen}{\ensuremath \mathscr{G}^R}  % script
\newcommand{\lGreen}{\ensuremath \mathscr{G}^<}  % script
\newcommand{\rgreen}{\ensuremath \mathbf{G}^R}
\newcommand{\lgreen}{\ensuremath \mathbf{G}^<}
\newcommand{\create}{\ensuremath \cqop{c}}  % creation operator
\newcommand{\destroy}{\ensuremath \qop{c}}  % destruction operator
\newcommand{\ccreate}[1]{\ensuremath \qop{ψ}^\dagger(#1)}  % creation operator
\newcommand{\cdestroy}[1]{\ensuremath \qop{ψ}(#1)}  % destruction operator
%% general stuff
\newcommand{\complex}{\mathbb{C}}
\newcommand{\reals}{\mathbb{R}}
\newcommand{\rationals}{\mathbb{Q}}
\newcommand{\integers}{\mathbb{Z}}
\newcommand{\naturals}{\mathbb{N}}
\newcommand{\unit}{\mathbb{1}}
\newcommand{\mb}[1]{\ensuremath \mathbf{#1}}
\newcommand{\mr}[1]{\ensuremath \mathrm{#1}}
\DeclareMathOperator{\variance}{var}
%% software names
\newcommand{\kwant}{\textsc{kwant}\ }
\newcommand{\tkwant}{\textsc{tkwant}\ }


\begin{document}
% --------------------------
% Front matter
% --------------------------
\pagenumbering{roman}            % roman page numbing (invisible for empty page style)
\pagestyle{empty}                % no header or footers
\input{content/titlepage}
\cleardoublepage

\pagestyle{plain}                % display just page numbers
\input{content/abstract}
\cleardoublepage
%
\input{content/acknowledgement}
\cleardoublepage
%
\setcounter{tocdepth}{\tocdepth}  % define depth of toc
\tableofcontents  % display table of contents
\cleardoublepage

% --------------------------
% Body matter
% --------------------------
\pagenumbering{arabic}    % arabic page numbering
\setcounter{page}{1}  % restart page numbering
\pagestyle{maincontentstyle}  % fancy header and footer

\input{content/summary.tex}
\input{content/summary_fr.tex}
\part{Numerical Algorithms and Software for Time-Resolved Quantum Transport}
\label{part:numerical}
\input{content/quantum-transport}
\input{content/source-sink}
\input{content/software}
\part{Applications of the Numerical Algorithms}
\label{part:applications}
\input{content/flying-qubit}
\input{content/josephson}
\input{content/majorana}
\input{content/conclusion}
\part{Appendix}
\label{part:appendix}
\appendix
\input{content/appendix}

\cleardoublepage

% --------------------------
% Back matter
% --------------------------
{%
\setstretch{1.1}
\renewcommand{\bibfont}{\normalfont\footnotesize}
%\setlength{\biblabelsep}{0pt}
\setlength{\bibitemsep}{0.5\baselineskip plus 0.5\baselineskip}
\printbibliography
}
\cleardoublepage

\input{content/publications}

\mbox{}

\end{document}
