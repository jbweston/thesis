# -*- coding: utf-8 -*-

PDF_VERSION ?= 1.4
PDFLATEX_OPTS=-interaction=errorstopmode -output-directory=build
TEX_SOURCES=$(wildcard content/*.tex)
STY_SOURCES=$(wildcard styles/*.sty)
BIB_SOURCES=references.bib special_references.bib
IMAGES = $(patsubst %.svg,%.pdf,$(shell find images -type f -name '*.svg'))
SOURCES=${TEX_SOURCES} ${STY_SOURCES} ${BIB_SOURCES} content/wire_example.py thesis.tex
GS_OPTS=-sDEVICE=pdfwrite -dBATCH -dNOPAUSE

thesis.pdf: ${SOURCES} ${IMAGES}
	test -d build || mkdir build
	yes x | xelatex ${PDFLATEX_OPTS} thesis.tex
	cp references.bib build/
	cp special_references.bib build/
	cd build && bibtex thesis && cd ..
	yes x | xelatex ${PDFLATEX_OPTS} thesis.tex > /dev/null
	mv build/thesis.pdf thesis.pdf

.PHONY: archive
archive: thesis.pdf
	gs ${GS_OPTS} -dCompatibilityLevel=${PDF_VERSION}\
				  -o thesis-archive.pdf $<

.PHONY: archive-nodeps
archive-nodeps:
	gs ${GS_OPTS} -dCompatibilityLevel=${PDF_VERSION}\
				  -o thesis-archive.pdf thesis.pdf

%.pdf : %.svg
	inkscape -Dz --export-pdf=$@ $<

clean:
	rm -rf build
	find images -type f -name '*.pdf' -exec rm {} \;
	rm -f thesis*.pdf
